package com.example.tucompa.ui.view

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.core.ActivityExtentions.createDialog
import com.example.tucompa.databinding.FragmentPreferredContactsBinding
import com.example.tucompa.databinding.FragmentRoutesHistoryBinding
import com.example.tucompa.domain.Route
import com.example.tucompa.domain.User
import com.example.tucompa.ui.view.adapters.AcceptedRequestAdapter
import com.example.tucompa.ui.view.adapters.PendingRequestAdapter
import com.example.tucompa.ui.view.adapters.RouteHistoryAdapter
import com.example.tucompa.ui.viewmodel.RoutesViewModel
import com.example.tucompa.ui.viewmodel.UserProfileViewModel
import com.example.tucompa.ui.viewmodel.UserRegistrationViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RoutesHistoryFragment : Fragment() {

    private lateinit var binding: FragmentRoutesHistoryBinding
    private var activity: AppActivity? = null

    private lateinit var connectivityLiveData: ConnectivityLiveData

    private lateinit var routesAdapter: RouteHistoryAdapter

    private val routesViewModel : RoutesViewModel by viewModels()
    private val userProfileViewModel : UserProfileViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRoutesHistoryBinding.inflate(inflater, container, false)

        activity = getActivity() as AppActivity

        connectivityLiveData = context?.let { ConnectivityLiveData(it) }!!
        connectivityLiveData.checkValidNetworks()

        val bar: View? = activity?.findViewById(R.id.lyBack)
        if (bar != null) {
            bar.visibility = View.VISIBLE
        }

        val btnBack: Button? = activity?.findViewById(R.id.btnBack) ?: null
        if (btnBack != null) {
            btnBack.visibility = View.VISIBLE
        }

        val tvTop: TextView? = activity?.findViewById(R.id.txtNavTittle)
        if (tvTop != null) {
            tvTop.text = "Historial de viajes"
        }

        addObservers()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    private fun setup() {
        routesAdapter = RouteHistoryAdapter(listOf(), mutableListOf(), User())

        binding.rvRoutes.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = routesAdapter
            Log.i("RoutesHistoryFragment", "Adapter set for rvRoutes")
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            if(connectivityLiveData.value != null && connectivityLiveData.value == true) {
                userProfileViewModel.currentUser(connectivityLiveData.value!!)
            }
            binding.swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun addObservers() {
        userProfileViewModel.currentUser(connectivityLiveData.value?: false)

        userProfileViewModel.currentUserModel.observe(viewLifecycleOwner) {
            Log.i("RoutesHistoryFragment", "Got current user $it")
            if(it != null) {
                routesAdapter.setActualUser(it)
                routesViewModel.getAllRoutesUserIsBeen(it.email, connectivityLiveData.value?: false)
            } else {

            }
        }

        routesViewModel.currentUserRoutes.observe(viewLifecycleOwner){
            Log.i("RoutesHistoryFragment", "Got current user routes $it")
            if(it.isNotEmpty()) {
                routesAdapter.addRoutes(it)
                (it as MutableList<Route>?)?.let { it1 ->
                    userProfileViewModel.getUsersFromRouteList(
                        it1, connectivityLiveData.value?: false)
                }
            } else {
                binding.tvNoHistory.visibility = View.VISIBLE
            }
        }

        userProfileViewModel.routesAndUsers.observe(viewLifecycleOwner) {
            Log.i("RoutesHistoryFragment", "Got current user routes admins $it")
            if(!it.isNullOrEmpty()) {
                routesAdapter.addAdminUser(it)
            }
        }

        connectivityLiveData.observe(viewLifecycleOwner) {
            if(it) {
                binding.tvNoInternet.visibility = View.GONE
            } else {
                binding.tvNoInternet.visibility = View.VISIBLE
            }
        }

        routesViewModel.loading.observe(viewLifecycleOwner) {
            if(it) {
                binding.pbLoading.visibility = View.VISIBLE
            } else {
                binding.pbLoading.visibility = View.GONE
            }
        }
    }
}