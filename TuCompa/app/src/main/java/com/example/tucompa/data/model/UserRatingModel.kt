package com.example.tucompa.data.model

data class UserRatingModel(
    val rate : Float = 0.0F,
    val pending : Boolean = true,
    val inadequate : Boolean = false,
    val userEmail: String = "",
)