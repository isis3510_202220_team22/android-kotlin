package com.example.tucompa.domain

import com.example.tucompa.data.database.entitites.CurrentUserEntity
import com.example.tucompa.data.database.entitites.UserEntity
import com.example.tucompa.data.model.UserModel
import com.google.firebase.Timestamp

data class User(
    val email: String = "",
    val image: String = "",
    val lastRoute: Timestamp = Timestamp.now(),
    val name: String = "",
    val quantityRating: Int = 0,
    val streak: Int = 0,
    val totalRating: Double = 0.0,
    val isInRoute : Boolean = false,
    val currentRoute : String = ""
)

fun UserModel.toDomain() = User(email, image, lastRoute, name, quantityRating, streak, totalRating, isInRoute, currentRoute)
fun UserEntity.toDomain() = User(email, image, Timestamp(lastRoute), name, quantityRating, streak, totalRating, isInRoute, currentRoute)
fun CurrentUserEntity.toDomain() = User(email, image, Timestamp(lastRoute), name, quantityRating, streak, totalRating, isInRoute, currentRoute)