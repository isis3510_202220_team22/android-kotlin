package com.example.tucompa

object Constants {

    //ERRORS
    //Auth errors
    const val EMPTY_EMAIL_ERROR : String = "empty_email_error"
    const val EMPTY_PASSWORD_ERROR : String  = "empty_password_error"
    const val NON_UNIANDES_EMAIL_ERROR : String  = "non_uniandes_email_error"
    const val NOT_EMAIL_ERROR : String  = "not_email_error"
    const val TOO_MANY_ATTEMPTS_ERROR : String = "We have blocked all requests from this device due to unusual activity. Try again later. [ Access to this account has been temporarily disabled due to many failed login attempts. You can immediately restore it by resetting your password or you can try again later. ]"
    const val INCORRECT_USER_OR_PASSSWORD: String = "The password is invalid or the user does not have a password."
    const val NON_EXISTENT_USER : String = "There is no user record corresponding to this identifier. The user may have been deleted."

    val SIGN_IN_ERRORS_MESSAGES = mapOf(
        EMPTY_EMAIL_ERROR to "Escribe un email",
        EMPTY_PASSWORD_ERROR to "Escribe una contraseña",
        NON_UNIANDES_EMAIL_ERROR to "El correo no está asociado a la Universidad de los Andes",
        NOT_EMAIL_ERROR to "Asegúrate que esto sea un correo electrónico",
        TOO_MANY_ATTEMPTS_ERROR to "Has realizado demasiados intentos, intente de nuevo más tarde",
        INCORRECT_USER_OR_PASSSWORD to "Usuario o contraseña incorrectos, verifíca tus datos",
        NON_EXISTENT_USER to "El usuario escrito no existe, verifíca tus datos"
    )

    const val LOGIN_CONNECTION_ERROR : String = "No tienes conexión a intenet, revisa tu conexión e intenta más tarde."

    //Create commute errors
    const val PAST_DEPARTURE_TIME_ERROR : String = "past_departure_time_error"
    const val NON_EXISTENT_ORIGIN_STATION_ERROR : String = "non_existent_origin_station_error"
    const val NON_EXISTENT_DESTINATION_STATION_ERROR : String = "non_existent_destination_station_error"
    const val SAME_STATION_ERROR : String = "same_station_error"

    val CREATE_COMMUTE_ERRORS_MESSAGES = mapOf(
        PAST_DEPARTURE_TIME_ERROR to "La hora de salida debe ser futura",
        NON_EXISTENT_ORIGIN_STATION_ERROR to "Escoge una estación de la lista",
        NON_EXISTENT_DESTINATION_STATION_ERROR to "Escoge una estación de la lista",
        SAME_STATION_ERROR to "La estación de origen y destino deben ser diferentes"
    )

    //Create report errors
    const val FUTURE_DEPARTURE_TIME_ERROR : String = "future_departure_time_error"
    const val NON_EXISTENT_STATION_ERROR : String = "non_existent_station_error"
    const val EMPTY_TYPE : String = "empty_type"
    const val EMPTY_DESCRIPTION : String = "empty_description"
    const val SHORT_DESCRIPTION : String = "short_desription"

    val CREATE_REPORT_ERRORS_MESSAGES = mapOf(
        FUTURE_DEPARTURE_TIME_ERROR to "La fecha y hora del reporte no puede ser futura",
        NON_EXISTENT_STATION_ERROR to "Escoge una estación de la lista",
        EMPTY_TYPE to "Escoge un tipo",
        EMPTY_DESCRIPTION to "Escribe una descripción",
        SHORT_DESCRIPTION to "Descripción muy corta"
    )

    const val WAGON_DOES_NOT_EXIST : String = "No existe el vagón"

    const val PHONE_NUMBER_LENGTH : String = "El número de teléfono debe tener 10 dígitos"
    const val EMPTY_NAME: String = "El nombre no debe estar vacío"
    const val EMPTY_IMAGE: String = "Debe escoger una imagen"
    const val ADD_PREFERRED_CONTACT_CONNECTION_ERROR: String = "Es posible que no se carguen las imágenes porque no hay conexión"

    const val SEARCH_CONNECTION_ERROR : String = "No se puede hacer la búsqueda sin conexión"
    const val SCAN_CONNECTION_ERROR : String = "No se puede hacer el escaneo sin conexión"
    const val REPORT_CONNECTION_ERROR : String = "No se puede publicar el reporte sin conexión"

    //Get reports errors
    const val EMPTY_STAION_NAME_ERROR : String = "empty_station_name_error"

    val GET_REPORTS_ERRORS_MESSAGES = mapOf(
        EMPTY_STAION_NAME_ERROR to "La estación está vacía, no se puede realizar la búsqueda"
    )

    //User rating errors
    const val ERROR_POSTING_USER_RATING : String = "error_posting_user_rating"

    val USER_RATING_ERRORS = mapOf(
        ERROR_POSTING_USER_RATING to "Hubo un error creando el UserRating"
    )

    //NAMES
    // Database documents names
    const val ROUTE_COLLECTION_NAME : String = "route"
    const val STATION_COLLECTION_NAME : String = "station"
    const val REPORT_COLLECTION_NAME : String = "reports"
    const val WAGON_COLLECTION_NAME : String = "wagons"
    const val USER_COLLECTION_NAME : String = "user"
    const val USER_RATING_COLLECTION_NAME : String = "userRating"


    //UTILS
    const val MINIMUM_REPORTS_FOR_ALERT : Int = 1

    //TRACES
    const val LOGIN_VIEW_TRACE : String = "login_view_trace"
    const val CREATE_COMMUTE_VIEW_TRACE : String = "create_commute_view_trace"
    const val COMMUTE_VIEW_TRACE : String = "commute_view_trace"
    const val RATING_VIEW_TRACE : String = "rating_view_trace"
    const val SEARCH_VIEW_TRACE : String = "search_view_trace"
    const val PROFILE_VIEW_TRACE : String = "profile_view_trace"
    const val CREATE_REPORT_VIEW_TRACE : String = "create_report_view_trace"
    const val QR_SCAN_VIEW_TRACE : String = "create_report_view_trace"
    const val PREFERRED_CONTACTS_VIEW_TRACE : String = "preferred_contacts_view_trace"
    const val ROUTES_HISTORY_VIEW_TRACE : String = "routes_history_view_trace"
}