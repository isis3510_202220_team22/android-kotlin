package com.example.tucompa.ui.view

import android.content.pm.PackageManager
import android.graphics.Rect
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.MotionEvent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.databinding.ActivityAppBinding
import com.example.tucompa.ui.viewmodel.RoutesViewModel
import com.example.tucompa.ui.viewmodel.UserProfileViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.google.firebase.perf.metrics.Trace
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class AppActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAppBinding
    lateinit var bottomNav : BottomNavigationView

    var actualView : String = VIEW_COMMUTE
    var routeId : String = ""
    var isInRoute : Boolean = false
    var finishedRoute : Boolean = false

    var lastTimeSuggested : Long = -1L
    var lastStationSugested : String = ""

    private val userProfileViewModel : UserProfileViewModel by viewModels()
    private val routesViewModel : RoutesViewModel by viewModels()
    private var snackbar : Snackbar? = null

    private lateinit var connectivityLiveData: ConnectivityLiveData

    private lateinit var actualTrace : Trace

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        connectivityLiveData = ConnectivityLiveData(this)

        if(savedInstanceState != null) {
            with(savedInstanceState) {
                routeId = getString(ROUTE_ID).toString()
                isInRoute = getBoolean(IS_IN_ROUTE)
                finishedRoute = getBoolean(FINISHED_ROUTE)
                actualView = getString(ACTUAL_VIEW).toString()
                lastTimeSuggested = getLong(LAST_TIME_SUGGESTED)
            }
            Log.i("AppActivity", "Detected SavedInstaceState: $routeId, $isInRoute, $finishedRoute, $actualView")
        }

        binding = ActivityAppBinding.inflate(layoutInflater)
        actualTrace = Firebase.performance.newTrace(Constants.CREATE_COMMUTE_VIEW_TRACE)
        setContentView(binding.root)

        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED )
        {
            ActivityCompat.requestPermissions(this,  arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION), 1)
        }

        reloadWithData()
        bottomNav = findViewById(R.id.bottomNav) as BottomNavigationView
        bottomNav.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.search -> {
                    reloadSearchItem()
                }
                R.id.commute -> {
                    reloadCommuteItem()
                }
                R.id.profile -> {
                    reloadProfileItem()
                }
            }
            true
        }

        snackbar = Snackbar.make(binding.lyBack,
            "No tienes conexión a internet, algunas funciones estarán limitadas",
            Snackbar.LENGTH_INDEFINITE)

        connectivityLiveData.checkValidNetworks()

        addObservers()
        setUp()
    }

    fun setAndStartActualTrace(traceId : String) {
        Log.i("AppActivity", "Starting trace $traceId")
        actualTrace = Firebase.performance.newTrace(traceId)
        actualTrace.start()
        Log.i("AppActivity", "Started trace $traceId")
    }

    fun stopActualTrace() {
        Log.i("AppActivity", "Stopping trace ${actualTrace.name}")
        actualTrace.stop()
        Log.i("AppActivity", "Stopped trace ${actualTrace.name}")
    }

    fun reloadCommuteItem() {
        actualView = VIEW_COMMUTE
        if(!isInRoute || routeId.isEmpty()) {
            isInRoute = false
            routeId = ""
            stopActualTrace()
            setAndStartActualTrace(Constants.CREATE_COMMUTE_VIEW_TRACE)
            replaceFragment(CreateCommuteFragment())
        } else if(!finishedRoute){
            stopActualTrace()
            setAndStartActualTrace(Constants.COMMUTE_VIEW_TRACE)
            replaceCommuteFragment(routeId)
        } else {
            stopActualTrace()
            setAndStartActualTrace(Constants.RATING_VIEW_TRACE)
            replaceUserRatingFragment(routeId)
        }
    }

    private fun reloadProfileItem() {
        actualView = VIEW_PROFILE
        stopActualTrace()
        setAndStartActualTrace(Constants.PROFILE_VIEW_TRACE)
        replaceFragment(ProfileFragment())
    }

    private fun reloadSearchItem() {
        actualView = VIEW_SEARCH
        stopActualTrace()
        setAndStartActualTrace(Constants.SEARCH_VIEW_TRACE)
        replaceFragment(SearchFragment())
    }

    private fun setUp() {
        binding.btnBack.setOnClickListener {
            this.getFragmentManager().popBackStack();
        }
    }

    private fun reloadActualFragment() {
        when(actualView) {
            VIEW_SEARCH -> {
                replaceFragment(SearchFragment())
            }
           VIEW_COMMUTE -> {
                reloadCommuteItem()
            }
            VIEW_PROFILE -> {
                replaceFragment(ProfileFragment())
            }
        }
    }

    private fun replaceCommuteFragment(routeId : String) {
        val frag : Fragment = CommuteFragment()
        val bundle : Bundle = Bundle()
        bundle.putString("routeId", routeId)
        frag.arguments = bundle
        replaceFragment(frag)
    }

    private fun replaceUserRatingFragment(routeId : String) {
        val frag : Fragment = UserRatingFragment()
        val bundle : Bundle = Bundle()
        bundle.putString("routeId", routeId)
        frag.arguments = bundle
        replaceFragment(frag)
    }

    private fun replaceFragment(fragment: Fragment){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }

    private fun addObservers() {
        userProfileViewModel.currentUserModel.observe(this) {
            if(it != null) {
                isInRoute = it.isInRoute
                routeId = it.currentRoute

                Log.i("AppActivity", "Read user $isInRoute, $routeId")

                reloadActualFragment()
            }
        }

        routesViewModel.lastRoute.observe(this) {
            this@AppActivity.finishedRoute = it.isFinished
            Log.i("AppActivity", "Read route $finishedRoute")
            reloadActualFragment()
        }

        connectivityLiveData.observe(this) {
            if(it) {
                if(actualView != VIEW_PROFILE) {
                    reloadWithData()
                }
            } else {
                showNoInternetSnackBar()
            }
        }
    }

    private fun reloadWithData() {
        getUser()
        getActualRoute()
        reloadActualFragment()
    }

    fun showNoInternetSnackBar() {
        if(snackbar?.isShown != true) {
            snackbar?.show()
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_DOWN) {
            if (snackbar != null && snackbar!!.isShown) {
                val sRect = Rect()
                snackbar?.getView()?.getHitRect(sRect)

                //This way the snackbar will only be dismissed if
                //the user clicks outside it.

                //This way the snackbar will only be dismissed if
                //the user clicks outside it.
                if (!sRect.contains(ev.x.toInt(), ev.y.toInt())) {
                    snackbar?.dismiss()
                    snackbar = null
                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }


    fun getUser() {
        userProfileViewModel.currentUser(connectivityLiveData.value?: false)
    }

    fun getActualRoute() {
        if(routeId.isNotEmpty()) {
            routesViewModel.getRoute(routeId, connectivityLiveData.value ?: false)
        }
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        outState.run {
            putString(ACTUAL_VIEW, actualView)
            putString(ROUTE_ID, routeId)
            putBoolean(IS_IN_ROUTE, isInRoute)
            putBoolean(FINISHED_ROUTE, finishedRoute)
            putLong(LAST_TIME_SUGGESTED, lastTimeSuggested)
            putString(LAST_STATION_SUGGESTED, lastStationSugested)
        }

        super.onSaveInstanceState(outState, outPersistentState)
    }

    companion object {
        const val ROUTE_ID = "routeId"
        const val IS_IN_ROUTE = "isInRoute"
        const val FINISHED_ROUTE = "finishedRoute"
        const val ACTUAL_VIEW = "actual_view"

        const val LAST_TIME_SUGGESTED = "last_time_suggested"
        const val LAST_STATION_SUGGESTED = "last_station_suggested"

        const val VIEW_PROFILE = "profile_view"
        const val VIEW_SEARCH = "search_view"
        const val VIEW_COMMUTE = "commute_view"
    }
}