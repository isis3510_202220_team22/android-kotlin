package com.example.tucompa.data.database.dao

import androidx.room.*
import com.example.tucompa.data.database.entitites.UserRatingEntity

@Dao
interface UserRatingDao {
    @Query("SELECT * FROM user_rating")
    suspend fun getAll(): List<UserRatingEntity>

    @Query("SELECT * FROM user_rating WHERE user_email = :userEmail")
    suspend fun getUserById(userEmail: String): UserRatingEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg userEntities: UserRatingEntity)

    @Query("DELETE FROM user_rating WHERE user_email = :userEmail")
    suspend fun deleteById(userEmail: String)

    @Delete
    suspend fun delete(userEntity: UserRatingEntity)
}