package com.example.tu

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tucompa.data.repository.RouteRepository
import com.example.tucompa.data.repository.StationRepository
import com.example.tucompa.domain.Route
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class StationViewModel @Inject constructor(private val stationRepository: StationRepository, private val routeRepository: RouteRepository) : ViewModel() {
    private val stationNamesList = MutableLiveData<List<String>>()
    val stationNames get() = stationNamesList

    private val frequentStationList = MutableLiveData<List<String>>()
    val frequentStations get() = frequentStationList

    fun getStationNames(isConnection : Boolean) = viewModelScope.launch {
        val stations = stationRepository.getStations(isConnection).map { station -> station.name }
        stationNamesList.postValue(stations)
    }

    fun getMostFrequentRoute(userEmail : String, latitude : Double, longitude : Double) = viewModelScope.launch {
        Log.i("StationViewModel", "Getting user $userEmail most frequent routes, it is in $latitude, $longitude")
        val list : List<String> = routeRepository.getMostCommonRoute(userEmail, latitude, longitude)
        Log.i("StationViewModel", "Got route $list")
        if(!list.isNullOrEmpty()) {
            frequentStationList.postValue(list)
        }
    }


}