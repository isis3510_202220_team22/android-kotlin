package com.example.tucompa.domain

import com.example.tucompa.data.database.entitites.CurrentReportEntity
import com.example.tucompa.data.model.CurrentReportModel
import com.google.firebase.Timestamp

data class CurrentReport(
    val dateTime : Timestamp,
    val description : String,
    val type : String,
    val station : String
)

fun CurrentReportModel.toDomain() = CurrentReport(dateTime, description, type, station)
fun CurrentReportEntity.toDomain() = CurrentReport(Timestamp(dateTime), description, type, station)
