package com.example.tucompa.ui.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tucompa.R
import com.example.tucompa.databinding.AcceptedRequestBinding
import com.example.tucompa.domain.User
import com.example.tucompa.domain.UserRating
import com.example.tucompa.ui.view.listeners.CommuteListener
import com.squareup.picasso.Picasso
import kotlin.math.round

class AcceptedRequestAdapter (private var requestList: MutableList<UserRating>,
                              private var userList: List<User>,
                              private var admin: Boolean,
                              private var actualUserEmail : String,
                              private var adminUserEmail: String,
                              private val listener: CommuteListener) :
    RecyclerView.Adapter<AcceptedRequestAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = AcceptedRequestBinding.bind(view)

        fun setListener(userRating: UserRating) {
            binding.btnMessage.setOnClickListener {
                listener.onMessageButton(userRating)
            }
            binding.btnDelete.setOnClickListener {
                listener.onDeleteButton(userRating)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.accepted_request, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.i("AcceptedRequestsAdapter", "Printing position position $position")
        val user : User = userList[position]
        val userRating = requestList[position]

        var name = user.name
        if(admin) {
            holder.binding.btnDelete.visibility = View.VISIBLE
        }

        if(user.email == actualUserEmail) {
            name += " (Tú)"
            holder.binding.btnDelete.visibility = View.GONE
            holder.binding.btnMessage.visibility = View.GONE
        }

        if(user.email == adminUserEmail) {
            name += " (Admin)"
        }

        holder.binding.profileName.text = name
        holder.binding.profileRating.text = ( round(user.totalRating*100/user.quantityRating)/100).toString()
        Picasso.get().load(user.image).into(holder.binding.profilePhoto)

        holder.setListener(userRating)
    }

    fun addRequests(newRequest:  MutableList<UserRating>, newUser: List<User>) {
        Log.i("AcceptedRequestsAdapter", "Updating data request: $newRequest; user: $newUser")
        requestList = newRequest
        userList = newUser
        Log.i("AcceptedRequestsAdapter", "Updated data request: $newRequest; user: $newUser")
        notifyDataSetChanged()
    }

    fun setAdmin(newAdminStatus: Boolean) {
        admin = newAdminStatus
        notifyDataSetChanged()
    }

    fun setActualUser(newActualUser : String) {
        actualUserEmail = newActualUser
        notifyDataSetChanged()
    }

    fun setAdminUser(newAdminUser : String) {
        adminUserEmail = newAdminUser
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = requestList.size
}