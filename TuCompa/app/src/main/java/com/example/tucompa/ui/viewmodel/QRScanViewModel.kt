package com.example.tucompa.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tucompa.Constants
import com.example.tucompa.data.repository.WagonRepository
import com.example.tucompa.domain.Wagon
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QRScanViewModel @Inject constructor(
    private val wagonRepository: WagonRepository,
    private val firebaseAnalytics: FirebaseAnalytics
) :
    ViewModel() {

    private val searchWagonEventsChannel = Channel<QRScanViewModel.AllEvents>()
    val allsearchWagonEventsFlow = searchWagonEventsChannel.receiveAsFlow()

    val wagonModel = MutableLiveData<Wagon?>()

    fun logQRScanViewOpened(userEmail: String) {
        firebaseAnalytics.logEvent("qr_scan_view_opened"){
            param("user_qr_view_opened", userEmail)
        }
    }

    fun currentWagon(text: String, userEmail: String) {
        firebaseAnalytics.logEvent("qr_scan_feature_used"){
            param("user_qr_feature_used", userEmail)
        }

        if ("-" in text) {
            val names: List<String> = text.split("-")
            val currentWagon = wagonRepository.getWagon(names[0], names[1])
            viewModelScope.launch {
                if (currentWagon == null) {
                    searchWagonEventsChannel.send(
                        AllEvents.Error(Constants.WAGON_DOES_NOT_EXIST)
                    )
                }
            }
            wagonModel.postValue(currentWagon)
        } else {
            viewModelScope.launch {
                searchWagonEventsChannel.send(
                    AllEvents.Error(Constants.WAGON_DOES_NOT_EXIST)
                )
            }
        }
    }

    sealed class AllEvents {
        data class Message(val message: String) : AllEvents()
        data class Error(val error: String) : AllEvents()
    }
}