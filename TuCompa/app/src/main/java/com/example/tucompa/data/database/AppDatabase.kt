package com.example.tucompa.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.tucompa.data.database.dao.*
import com.example.tucompa.data.database.entitites.*

@Database(
    entities = [UserEntity::class,
        CurrentUserEntity::class,
        StationEntity::class,
        RouteEntity::class,
        UserRatingEntity::class,
        CurrentReportEntity::class,
        PreferredContactEntity::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun currentUserDao(): CurrentUserDao
    abstract fun stationDao(): StationDao
    abstract fun routeDao(): RouteDao
    abstract fun userRatingDao(): UserRatingDao
    abstract fun currentReportDao(): CurrentReportDao
    abstract fun preferredContactDao(): PreferredContactDao
}