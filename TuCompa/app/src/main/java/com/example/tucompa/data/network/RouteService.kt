package com.example.tucompa.data.network

import android.content.ContentValues
import android.util.Log
import com.example.tucompa.Constants
import com.example.tucompa.data.model.RouteModel
import com.example.tucompa.data.model.UserRatingModel
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FieldPath.documentId
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class RouteService @Inject constructor(private val db: FirebaseFirestore) {

    val collectionRoutes = db.collection(Constants.ROUTE_COLLECTION_NAME)

    suspend fun createRoute(
        departureDate: Timestamp,
        destinationStationName: String,
        originStationName: String,
        userEmail: String
    ): RouteModel? {
        val routeData = mapOf(
            "departureDate" to departureDate,
            "destinationStationName" to destinationStationName,
            "originStationName" to originStationName,
            "userEmail" to userEmail,
            "isFinished" to false
        )

        var route: RouteModel? = null

        val job = GlobalScope.launch {
            collectionRoutes.add(routeData).addOnSuccessListener { documentReference ->
                route = RouteModel(
                    documentReference.id,
                    departureDate,
                    destinationStationName,
                    originStationName,
                    userEmail,
                    false
                )

                Log.i("Route Service:", "Successfully added route $route")
            }.addOnFailureListener {
                Log.e("Route Service:", "Failed to add route")
            }.await()
        }

        runBlocking {
            job.join()
        }

        return route
    }

    suspend fun getRoute(routeId: String): RouteModel? {
        var route: RouteModel? = null

        Log.i("RouteService", "Fetching $routeId")
        val job = GlobalScope.launch {
            route =
                collectionRoutes.document(routeId).get().await().toObject(RouteModel::class.java)
        }

        runBlocking {
            job.join()
        }

        Log.i("RouteService", "Returning route: $route")
        return route
    }

    suspend fun getAllRoutesFromUser(userEmail: String): List<RouteModel> {
        var routes: List<RouteModel> = listOf()

        val job = GlobalScope.launch {
            routes = collectionRoutes.whereEqualTo("userEmail", userEmail)
                .get()
                .await()
                .toObjects(RouteModel::class.java)
        }

        runBlocking {
            job.join()
        }

        return routes
    }

    suspend fun updateRouteIsFinished(routeId: String, newIsFinished: Boolean): RouteModel? {
        collectionRoutes.document(routeId).update("isFinished", true).addOnSuccessListener {
            Log.d(
                ContentValues.TAG,
                "Route Service: Route finished status successfully updated to $newIsFinished"
            )
        }.await()

        return getRoute(routeId)
    }

    suspend fun deleteRoute(routeId: String) {
        collectionRoutes.document(routeId).delete().addOnSuccessListener {
            Log.i("Route Service:", "Successfully deleted route $routeId")
        }.addOnFailureListener {
            Log.e("Route Service:", "Failed to delete route $routeId")
        }.await()
    }

    suspend fun getUserRatingsFromRoute(routeId: String): List<UserRatingModel> {
        Log.i("Route Service:", "Fetching route with id $routeId")
        val collectionUserRatings = collectionRoutes.document(routeId)
            .collection(Constants.USER_RATING_COLLECTION_NAME)


        var userRatings: List<UserRatingModel> = listOf()

        userRatings = collectionUserRatings.get().await().toObjects(UserRatingModel::class.java)

        return userRatings
    }

    suspend fun getUserRatingFromRoute(routeId: String, userEmail: String): UserRatingModel? {
        val collectionUserRatings = collectionRoutes.document(routeId)
            .collection(Constants.USER_RATING_COLLECTION_NAME)
            .document(userEmail)

        var userRating: UserRatingModel? = null

        collectionUserRatings.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val res = task.result
                if (res != null) {
                    userRating = res.toObject(UserRatingModel::class.java)
                }
            }
        }.await()

        return userRating
    }

    suspend fun postUserRating(
        routeId: String,
        pending: Boolean,
        userEmail: String
    ): UserRatingModel? {
        val collectionUserRatings = collectionRoutes.document(routeId)
            .collection(Constants.USER_RATING_COLLECTION_NAME)
        val userRatingData = mapOf(
            "rate" to 0.0F,
            "pending" to pending,
            "inadequate" to false,
            "userEmail" to userEmail
        )

        var userRating: UserRatingModel? = null
        collectionUserRatings.document(userEmail).set(userRatingData)
            .addOnSuccessListener {
                userRating = UserRatingModel(
                    0.0F,
                    pending,
                    false,
                    userEmail
                )
                Log.i(
                    "Route Service:",
                    "Successfully added userRating $userRating to route $routeId"
                )
            }.addOnFailureListener {
                Log.e("Route Service:", "Failed to add user rating to route $routeId")
            }.await()

        return userRating
    }

    fun getRoutes(): List<RouteModel> {
        val collectionRoutes = db.collection(Constants.ROUTE_COLLECTION_NAME)
        var routes = listOf<RouteModel>()

        val job = GlobalScope.launch(Dispatchers.IO) {
            routes = collectionRoutes.orderBy("departureDate", Query.Direction.DESCENDING).limit(50).get().await()
                .toObjects(RouteModel::class.java)
        }

        runBlocking {
            job.join()
        }

        return routes
    }

    fun getRoutesConditional(
        date: Timestamp?,
        destinationStationName: String?,
        originStationName: String?
    ): MutableList<RouteModel>? {

        val collectionRoutes = db.collection(Constants.ROUTE_COLLECTION_NAME)
        var routes = mutableListOf<RouteModel>()
        var date1 = Calendar.getInstance()
        date1.setTime(date?.toDate())
        date1.set(Calendar.SECOND, 0)
        date1.set(Calendar.MILLISECOND, 0)
        var date3 = Timestamp(Date(date1.timeInMillis))
        date1.add(Calendar.HOUR_OF_DAY, 1)
        var date2 = Timestamp(Date(date1.timeInMillis))

        if (date3 != null && date2 != null && destinationStationName != null && originStationName != null) {

            val job = GlobalScope.launch(Dispatchers.IO) {
                routes = collectionRoutes.whereGreaterThanOrEqualTo("departureDate", date3)
                    .whereLessThanOrEqualTo("departureDate", date2)
                    .whereEqualTo("destinationStationName", destinationStationName)
                    .whereEqualTo("originStationName", originStationName).get().await()
                    .toObjects(RouteModel::class.java)
                Log.d("RouteService", "DocumentSnapshot data : ${routes}")
            }

            runBlocking {
                job.join()
            }

        } else if (date3 == null && destinationStationName != null && originStationName != null) {

            val job = GlobalScope.launch(Dispatchers.IO) {
                collectionRoutes.whereEqualTo("destinationStationName", destinationStationName)
                    .whereEqualTo("originStationName", originStationName).get().await()
                    .toObjects(RouteModel::class.java)
                Log.d("RouteService", "DocumentSnapshot data : ${routes}")
            }

            runBlocking {
                job.join()
            }

        } else if (date3 == null && destinationStationName == null && originStationName != null) {

            val job = GlobalScope.launch(Dispatchers.IO) {
                collectionRoutes.whereEqualTo("originStationName", originStationName).get().await()
                    .toObjects(RouteModel::class.java)
                Log.d("RouteService", "DocumentSnapshot data : ${routes}")
            }

            runBlocking {
                job.join()
            }

        } else if (date3 == null && destinationStationName != null && originStationName == null) {

            val job = GlobalScope.launch(Dispatchers.IO) {
                collectionRoutes.whereEqualTo("destinationStationName", destinationStationName)
                    .get().await()
                    .toObjects(RouteModel::class.java)
                Log.d("RouteService", "DocumentSnapshot data : ${routes}")
            }

            runBlocking {
                job.join()
            }

        } else if (date3 != null && destinationStationName == null && originStationName != null) {

            val job = GlobalScope.launch(Dispatchers.IO) {
                collectionRoutes.whereGreaterThanOrEqualTo("departureDate", date3)
                    .whereLessThanOrEqualTo("departureDate", date2)
                    .whereEqualTo("originStationName", originStationName).get().await()
                    .toObjects(RouteModel::class.java)
                Log.d("RouteService", "DocumentSnapshot data : ${routes}")
            }

            runBlocking {
                job.join()
            }

        } else if (date3 != null && destinationStationName == null && originStationName == null) {

            val job = GlobalScope.launch(Dispatchers.IO) {
                collectionRoutes.whereGreaterThanOrEqualTo("departureDate", date3)
                    .whereLessThanOrEqualTo("departureDate", date2)
                    .get().await()
                    .toObjects(RouteModel::class.java)
                Log.d("RouteService", "DocumentSnapshot data : ${routes}")
            }

            runBlocking {
                job.join()
            }

        } else if (date3 != null && destinationStationName != null && originStationName == null) {

            val job = GlobalScope.launch(Dispatchers.IO) {
                collectionRoutes.whereGreaterThanOrEqualTo("departureDate", date3)
                    .whereLessThanOrEqualTo("departureDate", date2)
                    .whereEqualTo("destinationStationName", destinationStationName).get().await()
                    .toObjects(RouteModel::class.java)
                Log.d("RouteService", "DocumentSnapshot data : ${routes}")
            }

            runBlocking {
                job.join()
            }

        } else {
            routes = getRoutes().toMutableList()
        }

        return routes
    }

    suspend fun updateUserRatingPending(
        routeId: String,
        userEmail: String,
        newPendingStatus: Boolean
    ): UserRatingModel? {
        val collectionUserRatings = collectionRoutes.document(routeId)
            .collection(Constants.USER_RATING_COLLECTION_NAME)

        collectionUserRatings.document(userEmail).update("pending", newPendingStatus)
            .addOnSuccessListener {
                Log.d(
                    ContentValues.TAG,
                    "Route Service: UserRating pending status successfully updated to $newPendingStatus"
                )
            }.await()

        return getUserRatingFromRoute(routeId, userEmail)
    }

    suspend fun updateUserRatingRate(
        routeId: String,
        userEmail: String,
        newRate: Float
    ): UserRatingModel? {
        val collectionUserRatings = collectionRoutes.document(routeId)
            .collection(Constants.USER_RATING_COLLECTION_NAME)

        collectionUserRatings.document(userEmail).update("rate", newRate)
            .addOnSuccessListener {
                Log.d(
                    ContentValues.TAG,
                    "Route Service: UserRating rate successfully updated to $newRate"
                )
            }.await()

        return getUserRatingFromRoute(routeId, userEmail)
    }

    suspend fun updateUserRatingInadequate(
        routeId: String,
        userEmail: String,
        inadequate: Boolean
    ): UserRatingModel? {
        val collectionUserRatings = collectionRoutes.document(routeId)
            .collection(Constants.USER_RATING_COLLECTION_NAME)

        collectionUserRatings.document(userEmail).update("inadequate", inadequate)
            .addOnSuccessListener {
                Log.d(
                    ContentValues.TAG,
                    "Route Service: UserRating inadequate successfully updated to $inadequate"
                )
            }.await()

        return getUserRatingFromRoute(routeId, userEmail)
    }

    suspend fun deleteUserRating(routeId: String, userEmail: String) {
        val collectionUserRatings = collectionRoutes.document(routeId)
            .collection(Constants.USER_RATING_COLLECTION_NAME)

        collectionUserRatings.document(userEmail).delete().addOnSuccessListener {
            Log.i(
                "Route Service:",
                "Successfully deleted user rating $userEmail from route $routeId"
            )
        }.addOnFailureListener {
            Log.e("Route Service:", "Failed to delete user rating $userEmail from route $routeId")
        }.await()
    }

}