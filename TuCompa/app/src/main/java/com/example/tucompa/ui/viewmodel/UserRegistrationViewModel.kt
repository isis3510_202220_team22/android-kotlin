package com.example.tucompa.ui.viewmodel

import android.util.Log
import android.util.Patterns
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tucompa.Constants
import com.example.tucompa.data.repository.BaseAuthenticator
import com.example.tucompa.data.repository.UserRepository
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserRegistrationViewModel @Inject constructor(
    private val authenticator: BaseAuthenticator,
    private val userRepository: UserRepository) : ViewModel() {

    private val loggedUser = MutableLiveData<FirebaseUser?>()
    val currentUser get() = loggedUser

    private val isLoading = MutableLiveData<Boolean>()
    val loading get() = isLoading

    private val eventsChannel = Channel<AllEvents>()

    val allEventsFlow = eventsChannel.receiveAsFlow()

    fun validateAndSignInUser(email: String , password: String) = viewModelScope.launch{
        isLoading.postValue(true)
        var pat = Patterns.EMAIL_ADDRESS

        when {
            email.isEmpty() -> {
                eventsChannel.send(AllEvents.Error(Constants.EMPTY_EMAIL_ERROR))
                loading.postValue(false)
            }
            password.isEmpty() -> {
                eventsChannel.send(AllEvents.Error(Constants.EMPTY_PASSWORD_ERROR))
                loading.postValue(false)
            }
            !pat.matcher(email).matches() -> {
                eventsChannel.send(AllEvents.Error(Constants.NOT_EMAIL_ERROR))
                loading.postValue(false)
            }
            !email.endsWith("@uniandes.edu.co") -> {
                eventsChannel.send(AllEvents.Error(Constants.NON_UNIANDES_EMAIL_ERROR))
                loading.postValue(false)
            }
            else -> {
                signInUser(email , password)
            }
        }

        loading.postValue(false)
    }

    private fun signInUser(email:String, password: String) = viewModelScope.launch {
        try {
            val user = authenticator.signInWithEmailPassword(email, password)
            user?.let {
                loggedUser.postValue(it)
                eventsChannel.send(AllEvents.Message("Login: Success"))
            }
        } catch(e:Exception) {
            val error = e.toString().split(":").toTypedArray()
            Log.d("ERROR", "SignInError: ${error[1]}")
            eventsChannel.send(AllEvents.LoginError(error[1]))
        } finally {
            isLoading.postValue(false)
        }
    }

    fun getCurrentUser() = viewModelScope.launch {
        val user = authenticator.getUser()
        loggedUser.postValue(user)
    }

    sealed class AllEvents {
        data class Message(val message : String) : AllEvents()
        data class Error(val error : String) : AllEvents()
        data class LoginError(val error: String) : AllEvents()
    }

}