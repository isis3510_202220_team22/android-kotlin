package com.example.tucompa.domain

import com.example.tucompa.data.database.entitites.PreferredContactEntity
import com.example.tucompa.data.model.PreferredContactModel

data class PreferredContact(
    val name: String = "",
    val number: String = "",
    val image: String = ""
)

fun PreferredContactModel.toDomain() = PreferredContact(name, number, image)
fun PreferredContactEntity.toDomain() = PreferredContact(name, number, image)