package com.example.tucompa.data.model

data class WagonModel(
    val description: String = "",
    val id: String = "",
    val stationName: String = ""
)