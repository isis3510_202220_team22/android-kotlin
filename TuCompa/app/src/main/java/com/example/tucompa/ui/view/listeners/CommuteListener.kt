package com.example.tucompa.ui.view.listeners

import com.example.tucompa.domain.UserRating

interface CommuteListener {
    fun onAcceptButton(userRating: UserRating)
    fun onDeclineButton(userRating: UserRating)
    fun onMessageButton(userRating: UserRating)
    fun onDeleteButton(userRating: UserRating)
}