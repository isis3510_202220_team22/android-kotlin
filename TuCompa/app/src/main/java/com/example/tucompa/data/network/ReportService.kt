package com.example.tucompa.data.network

import android.content.ContentValues.TAG
import android.util.Log
import com.example.tucompa.Constants
import com.example.tucompa.data.model.ReportModel
import com.example.tucompa.data.model.RouteModel
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.toObjects
import kotlinx.coroutines.tasks.await
import java.util.*
import javax.inject.Inject

class ReportService @Inject constructor(private val db : FirebaseFirestore) {
    val collectionStation = db.collection(Constants.STATION_COLLECTION_NAME)

    suspend fun createStationReport(
        dateTime: Timestamp,
        description: String,
        type: String,
        station: String
    ): ReportModel {

        val stationReportData = mapOf(
            "dateTime" to dateTime,
            "description" to description,
            "type" to type
        )

        collectionStation.document(station)
            .collection(Constants.REPORT_COLLECTION_NAME).document().set(stationReportData).await()

        return ReportModel(
            dateTime,
            description,
            type
        )
    }

    suspend fun getReportsFromLastHourByStation(stationName : String): List<ReportModel>? {
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY)-1)
        val docRef = collectionStation.document(stationName).collection(Constants.REPORT_COLLECTION_NAME).whereGreaterThanOrEqualTo("dateTime", Timestamp(Date(cal.timeInMillis)))

        var reports : List<ReportModel>? = null
        docRef.get().addOnCompleteListener { task ->
            if(task.isSuccessful) {
                val res = task.result
                if(res != null) {
                    reports = res.toObjects(ReportModel::class.java)
                }
            }
        }.await()
        return reports
    }
}