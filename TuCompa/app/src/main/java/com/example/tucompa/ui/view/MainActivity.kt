package com.example.tucompa.ui.view

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.core.ActivityExtentions.createDialog
import com.example.tucompa.databinding.ActivityMainBinding
import com.example.tucompa.ui.view.adapters.CarouselRVAdapter
import com.example.tucompa.ui.viewmodel.UserProfileViewModel
import com.example.tucompa.ui.viewmodel.UserRegistrationViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val userRegistrationViewModel : UserRegistrationViewModel by viewModels()
    private val userProfileViewModel : UserProfileViewModel by viewModels()

    private lateinit var connectivityLiveData: ConnectivityLiveData

    private var isLoginIn : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.pbLoading.visibility = View.GONE

        connectivityLiveData = ConnectivityLiveData(this)
        connectivityLiveData.checkValidNetworks()

        lifecycleScope.launch {
            userRegistrationViewModel.allEventsFlow.collect { event ->
                when(event){
                    is UserRegistrationViewModel.AllEvents.Error -> {
                        when(event.error) {
                            Constants.EMPTY_EMAIL_ERROR -> binding.tilEmail.run {
                                error = Constants.SIGN_IN_ERRORS_MESSAGES[Constants.EMPTY_EMAIL_ERROR]
                                requestFocus()
                            }
                            Constants.NOT_EMAIL_ERROR -> binding.tilEmail.run {
                                error = Constants.SIGN_IN_ERRORS_MESSAGES[Constants.NOT_EMAIL_ERROR]
                                requestFocus()
                            }
                            Constants.NON_UNIANDES_EMAIL_ERROR -> binding.tilEmail.run {
                                error = Constants.SIGN_IN_ERRORS_MESSAGES[Constants.NON_UNIANDES_EMAIL_ERROR]
                                requestFocus()
                            }
                            Constants.EMPTY_PASSWORD_ERROR -> binding.tilPassword.run {
                                error = Constants.SIGN_IN_ERRORS_MESSAGES[Constants.EMPTY_PASSWORD_ERROR]
                                requestFocus()
                            }
                        }
                    }
                    is UserRegistrationViewModel.AllEvents.LoginError -> {
                        var message = "Hubo un error en la solicitud, por favor revise su conexión e intente de nuevo"
                        if(event.error.contains(Constants.TOO_MANY_ATTEMPTS_ERROR))
                        {
                            message = Constants.SIGN_IN_ERRORS_MESSAGES[Constants.TOO_MANY_ATTEMPTS_ERROR].toString()
                        }
                        else if(event.error.contains(Constants.INCORRECT_USER_OR_PASSSWORD)) {
                            message = Constants.SIGN_IN_ERRORS_MESSAGES[Constants.INCORRECT_USER_OR_PASSSWORD].toString()
                        }
                        else if(event.error.contains(Constants.NON_EXISTENT_USER)) {
                            message = Constants.SIGN_IN_ERRORS_MESSAGES[Constants.NON_EXISTENT_USER].toString()
                        }
                        binding.tilPassword.requestFocus()
                        showDefaultDialog(message)
                    }
                    is UserRegistrationViewModel.AllEvents.Message -> {
                        createToast("Login Existoso!")
                        launchAppActivityIntent()
                    }
                }
            }
        }

        userProfileViewModel.currentUser(connectivityLiveData.value?: false)
        addObservers()
        setup()
    }

    private fun setup() {
        with(binding) {
            viewPager.apply {
                clipChildren = false  // No clipping the left and right items
                clipToPadding = false  // Show the viewpager in full width without clipping the padding
                offscreenPageLimit = 3  // Render the left and right items
                (getChildAt(0) as RecyclerView).overScrollMode =
                    RecyclerView.OVER_SCROLL_NEVER // Remove the scroll effect
            }

            val demoData = arrayListOf(
                getString(R.string.login_carousel_1),
                getString(R.string.login_carousel_2),
                getString(R.string.login_carousel_3),
                getString(R.string.login_carousel_4)
            )

            viewPager.adapter = CarouselRVAdapter(demoData)

            btnLogin.setOnClickListener {
                showFormLogin()
            }

            btnSubmit.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()

                clearErrors()
                userRegistrationViewModel.validateAndSignInUser(email,password)
            }
        }
    }

    fun addObservers() {

        userRegistrationViewModel.loading.observe(this) {
            Log.i("MainActivity", "Changed loading status to $it")
            if (it) {
                binding.pbLoading.visibility = View.VISIBLE
                binding.viewPager.visibility =  View.GONE
                binding.llyFormLogin.visibility = View.GONE
            } else {
                binding.pbLoading.visibility = View.GONE
                if(!isLoginIn) {
                    Log.i("MainActivity", "Not login in, making view pager visible")
                    binding.viewPager.visibility =  View.VISIBLE
                } else {
                    Log.i("MainActivity", "Login in, making form visible")
                    binding.llyFormLogin.visibility = View.VISIBLE
                }
            }
        }

        userProfileViewModel.loading.observe(this) {
            Log.i("MainActivity", "Changed loading status to $it, and login is $isLoginIn")
            if (it) {
                binding.pbLoading.visibility = View.VISIBLE
                binding.viewPager.visibility =  View.GONE
                binding.llyFormLogin.visibility = View.GONE
            } else {
                binding.pbLoading.visibility = View.GONE
                if(!isLoginIn) {
                    Log.i("MainActivity", "Not login in, making view pager visible")
                    binding.viewPager.visibility =  View.VISIBLE
                } else {
                    Log.i("MainActivity", "Login in, making form visible")
                    binding.llyFormLogin.visibility = View.VISIBLE
                }
            }
        }

        userProfileViewModel.currentUserModel.observe(this){
            Log.i("MainActivity", "Got user $it")
            it?.let {
                Log.i("MainActivity", "Detected ussr, launching AppActivity")
                launchAppActivityIntent()
            }
        }

        connectivityLiveData.observe(this) { isNetwork ->
            if (!isNetwork) {
                binding.btnLogin.setOnClickListener {
                    this@MainActivity.createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
                }

                binding.btnSubmit.setOnClickListener {
                    this@MainActivity.createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
                }
            } else {
                binding.btnLogin.setOnClickListener {
                    showFormLogin()
                }

                binding.btnSubmit.setOnClickListener {
                    binding.pbLoading.visibility = View.VISIBLE
                    val email = binding.etEmail.text.toString()
                    val password = binding.etPassword.text.toString()

                    clearErrors()
                    userRegistrationViewModel.validateAndSignInUser(email,password)
                    binding.pbLoading.visibility = View.GONE
                }
            }
        }

        connectivityLiveData.checkValidNetworks()
    }

    private fun showFormLogin() {
        isLoginIn = true
        Log.i("MainActivity", "Showing form login, isLoginIn set to $isLoginIn")
        with(binding) {
            viewPager.visibility = View.GONE
            llyFormLogin.visibility =View.VISIBLE
            btnLogin.visibility = View.GONE
            btnSubmit.visibility = View.VISIBLE
        }
    }

    private fun showDefaultDialog(message: String) {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.apply {
            setTitle("Error")
            setMessage(message).setNeutralButton("Ok",
                DialogInterface.OnClickListener { dialog, id ->
                    dialog.dismiss()
                })
        }.create().show()
    }

    private fun clearErrors() {
        with(binding) {
            tilEmail.error = null
            tilPassword.error = null
        }
    }

    private fun createToast(message : String) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
        toast.show()
    }

    private fun launchAppActivityIntent() {
        val intent = Intent(this, AppActivity::class.java)
        launchIntent(intent)
    }
    private fun launchIntent(intent: Intent) {
        if(intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        } else {
            Toast.makeText(this, getString(R.string.profile_error_no_resolve), Toast.LENGTH_SHORT).show()
        }

    }
}