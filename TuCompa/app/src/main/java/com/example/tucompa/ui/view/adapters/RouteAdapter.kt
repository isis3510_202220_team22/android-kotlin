package com.example.tucompa.ui.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tucompa.R
import com.example.tucompa.databinding.ItemRouteBinding
import com.example.tucompa.domain.Route
import com.example.tucompa.domain.User
import com.example.tucompa.domain.UserRating
import com.example.tucompa.ui.view.listeners.CreateRatingListener
import com.squareup.picasso.Picasso
import java.text.DecimalFormat
import java.util.*

class RouteAdapter(
    private var routeList: MutableList<Route>,
    private var userList: MutableList<User>,
    private var actualUser: User,
    private var userRatingsAllRoutes: MutableList<List<UserRating>>,
    private val listener: CreateRatingListener
) : RecyclerView.Adapter<RouteAdapter.RouteViewHolder>() {

    inner class RouteViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val binding = ItemRouteBinding.bind(view)

        fun render(route: Route, user: User, state: Int) {
            // state -> [(0 : route owner or user is in route);
            // (1 : there are no ratings on the route or the user hasn't sent a rating yet);
            // (2 : the user has sent a rating but it hasn't been accepted);
            // (3 : the user rating has been accepted and he can go to route)]

            val cal = Calendar.getInstance()
            cal.time = route.departureDate.toDate()
            val hora = cal.get(Calendar.HOUR_OF_DAY)
            val minuto = cal.get(Calendar.MINUTE)

            binding.routeowner.text = user.name
            binding.routehour.text = "${checkDigit(hora)}:${checkDigit(minuto)}"
            binding.routerating.text = "${DecimalFormat("#.##").format(user.totalRating/user.quantityRating)}"
            Picasso.get().load(user.image).into(binding.routePhoto)

            when (state) {
                1 -> {
                    binding.btnRating.visibility = View.VISIBLE
                    binding.btnWaiting.visibility = View.GONE
                    binding.btnGoToCommute.visibility = View.GONE
                }
                2 -> {
                    binding.btnRating.visibility = View.GONE
                    binding.btnWaiting.visibility = View.VISIBLE
                    binding.btnGoToCommute.visibility = View.GONE
                }
                3 -> {
                    binding.btnRating.visibility = View.GONE
                    binding.btnWaiting.visibility = View.GONE
                    binding.btnGoToCommute.visibility = View.VISIBLE
                }
                else -> {}
            }

            binding.btnRating.setOnClickListener {
                listener.onCreateRating(route.id, true, actualUser.email)
            }

            binding.btnGoToCommute.setOnClickListener {
                listener.onGoingToCommute(route.id)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RouteViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return RouteViewHolder(layoutInflater.inflate(R.layout.item_route, parent, false))
    }

    override fun onBindViewHolder(holder: RouteViewHolder, position: Int) {
        val itemRoute = routeList.get(position)
        val itemUser = userList.get(position)
        var encontrado = false
        var choice = 0

        if (itemUser != null) {
            if (userRatingsAllRoutes.size != 0) {
                if (actualUser.email == itemUser.email || actualUser.isInRoute) {
                    choice = 0
                } else {
                    val itemRatingList = userRatingsAllRoutes.get(position)
                    if (itemRatingList != null) {
                        itemRatingList.forEach {
                            if(!encontrado){
                                if (actualUser.email == it.userEmail) {
                                    if (it.pending) {
                                        choice = 2
                                        encontrado = true
                                    } else {
                                        choice = 3
                                        encontrado = true
                                    }
                                } else {
                                    choice = 1
                                }
                            }
                        }
                    } else {
                        choice = 1
                    }
                }
            } else {
                if (actualUser.email == itemUser.email || actualUser.isInRoute) {
                    choice = 0
                } else {
                    choice = 1
                }
            }
        }

        holder.render(itemRoute, itemUser, choice)
    }

    override fun getItemCount(): Int = routeList.size

    fun setActualRouteAndUserList(
        newRouteList: MutableList<Route>,
        newUserList: MutableList<User>
    ) {
        routeList = newRouteList
        userList = newUserList
        notifyDataSetChanged()
    }

    fun setActualUser(newActualUser: User) {
        actualUser = newActualUser
        notifyDataSetChanged()
    }

    fun setActualUserRatingsAllRoutes(newActualUserRatingsAllRoutes: MutableList<List<UserRating>>) {
        userRatingsAllRoutes = newActualUserRatingsAllRoutes
        notifyDataSetChanged()
    }

    private fun checkDigit(number: Int): String {
        var response = ""
        if (number <= 9) {
            response = "0${number}"
        } else {
            response = "${number}"
        }
        return response
    }

}