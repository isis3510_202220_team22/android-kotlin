package com.example.tucompa.ui.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.compose.ui.unit.Constraints
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.databinding.FragmentProfileBinding
import com.example.tucompa.ui.viewmodel.UserProfileViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Math.round
import java.text.DecimalFormat

@AndroidEntryPoint
class ProfileFragment : Fragment(R.layout.fragment_profile) {
    private lateinit var binding: FragmentProfileBinding
    private var activity: AppActivity? = null
    private val userProfileViewModel: UserProfileViewModel by viewModels()

    private lateinit var connectivityLiveData: ConnectivityLiveData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater, container, false)

        activity = getActivity() as AppActivity

        connectivityLiveData = ConnectivityLiveData(requireActivity())
        connectivityLiveData.checkValidNetworks()

        setup()
        return binding?.root
    }

    private fun setup() {
        userProfileViewModel.currentUser(connectivityLiveData.value?: false)

        binding.apply {
            userProfileViewModel.currentUserModel.observe(viewLifecycleOwner, Observer {
                if (it != null) {
                    profileName.text = it.name
                    profileEmail.text = it.email
                    profileRating.text = "${DecimalFormat("#.##").format(it.totalRating/it.quantityRating)} - "
                    streakValue.text = "${it.streak} dia(s)"
                    Picasso.get().load(it.image).into(profilePhoto)
                } else {
                    val intent = Intent(context, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
            })

            val bar: View? = activity?.findViewById(R.id.lyBack) ?: null
            if (bar != null) {
                bar.visibility = View.GONE
            }

            val tvTop: TextView? = activity?.findViewById(R.id.txtNavTittle) ?: null
            if (tvTop != null) {
                tvTop.text = "Perfil"
            }

            scanButton.setOnClickListener {
                activity?.stopActualTrace()
                activity?.setAndStartActualTrace(Constants.QR_SCAN_VIEW_TRACE)
                launchScanActivityIntent()
            }

            reportButton.setOnClickListener {
                activity?.stopActualTrace()
                activity?.setAndStartActualTrace(Constants.CREATE_REPORT_VIEW_TRACE)
                replaceFragment(StationReportFragment())
            }

            history.setOnClickListener {
                activity?.stopActualTrace()
                activity?.setAndStartActualTrace(Constants.ROUTES_HISTORY_VIEW_TRACE)
                replaceFragment(RoutesHistoryFragment())
            }

            favoriteContacts.setOnClickListener {
                activity?.stopActualTrace()
                activity?.setAndStartActualTrace(Constants.PREFERRED_CONTACTS_VIEW_TRACE)
                replaceFragment(PreferredContactsFragment())
            }

            logout.setOnClickListener {
                activity?.stopActualTrace()
                userProfileViewModel.signOut()
            }

            userProfileViewModel.loading.observe(viewLifecycleOwner) {
                updateLoadingBar(it)
            }

        }
    }

    private fun launchScanActivityIntent() {
        val intent = Intent(activity, ScanQRCodeFragment::class.java)
        startActivity(intent)
    }

    private fun replaceFragment(fragment: Fragment){
        val fragmentTransaction = getActivity()?.supportFragmentManager?.beginTransaction()
        fragmentTransaction?.replace(R.id.container, fragment)
        fragmentTransaction?.addToBackStack(null)
        fragmentTransaction?.commit()
    }

    private fun updateLoadingBar(isLoading : Boolean) {
        with(binding){
            if(isLoading){
                pbLoading.visibility = View.VISIBLE
            } else {
                pbLoading.visibility = View.GONE
            }
        }
    }
}