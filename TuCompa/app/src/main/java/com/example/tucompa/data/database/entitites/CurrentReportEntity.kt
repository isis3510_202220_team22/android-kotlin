package com.example.tucompa.data.database.entitites

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.tucompa.data.model.CurrentReportModel
import java.util.*

@Entity(tableName = "current_report")
data class CurrentReportEntity(
    @PrimaryKey val dateTime: Date,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "station") val station: String,
)

fun CurrentReportModel.toCurrentReportEntity() = CurrentReportEntity(dateTime.toDate(), description, type, station)