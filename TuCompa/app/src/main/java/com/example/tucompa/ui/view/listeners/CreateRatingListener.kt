package com.example.tucompa.ui.view.listeners

interface CreateRatingListener {
    fun onCreateRating (routeId: String, pending: Boolean, userEmail: String)
    fun onGoingToCommute(routeId: String)
}