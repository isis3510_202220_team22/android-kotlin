package com.example.tucompa.ui.view

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.core.ActivityExtentions.createDialog
import com.example.tucompa.databinding.FragmentUserRatingBinding
import com.example.tucompa.domain.UserRating
import com.example.tucompa.ui.view.adapters.UserForRateAdapter
import com.example.tucompa.ui.view.listeners.RatingListener
import com.example.tucompa.ui.viewmodel.RoutesViewModel
import com.example.tucompa.ui.viewmodel.UserProfileViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserRatingFragment : Fragment(), RatingListener {

    lateinit var binding: FragmentUserRatingBinding
    private lateinit var activity: AppActivity

    private val userProfileViewModel : UserProfileViewModel by viewModels()
    private val routesViewModel : RoutesViewModel by viewModels()

    private lateinit var userForRateAdapter: UserForRateAdapter

    private lateinit var connectivityLiveData: ConnectivityLiveData

    private var ratings: MutableMap<String,Float> = mutableMapOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentUserRatingBinding.inflate(inflater, container,false)
        activity = getActivity() as AppActivity

        connectivityLiveData = ConnectivityLiveData(requireActivity())
        connectivityLiveData.checkValidNetworks()

        val bar: View? = activity.findViewById(R.id.lyBack) ?: null
        if (bar != null) {
            bar.visibility = View.VISIBLE
        }

        val btnBack: Button? = activity.findViewById(R.id.btnBack) ?: null
        if (btnBack != null) {
            btnBack.visibility = View.GONE
        }

        val tvTop: TextView? = activity.findViewById(R.id.txtNavTittle) ?: null
        if (tvTop != null) {
            tvTop.text = "Califica Tus Acompañantes"
        }

        getData()
        return binding.root
    }

    private fun  getRouteIdFromArguments() : String {
        val routeId = this.arguments?.get("routeId").toString()
        return routeId
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userForRateAdapter = UserForRateAdapter(mutableListOf(), listOf(), "", this)

        with(binding) {

            rvUserRating.apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = userForRateAdapter
                Log.i("UserRatingView", "Adapter set for rvUserRating")
            }

            btnSubmitRatings.setOnClickListener {
                val userList = userProfileViewModel.acceptedUsers.value ?: listOf()
                val actualUser = userProfileViewModel.currentUserModel.value?.email ?: ""
                userList.forEach { user ->
                    if(user.email != actualUser) {
                        userProfileViewModel.updateRate(user.email, ratings[user.email] ?: 0F)
                        routesViewModel.updateUserRatingRate(getRouteIdFromArguments(), user.email, ratings[user.email] ?: 0F)
                    } else {
                        userProfileViewModel.updateIsInRoute(user.email, false, "")
                    }

                }

                activity.getUser()
                activity.apply {
                    finishedRoute = false
                }
                activity.reloadCommuteItem()
            }

            btnSubmitAlone.setOnClickListener {
                val actualUser = userProfileViewModel.currentUserModel.value?.email ?: ""
                userProfileViewModel.updateIsInRoute(actualUser, false, "")

                activity.apply {
                    routeId = ""
                    isInRoute = false
                    finishedRoute = false
                }

                activity.reloadCommuteItem()
            }

            binding.swipeRefreshLayout.setOnRefreshListener {
                if(connectivityLiveData.value == true) {
                    userProfileViewModel.currentUser(connectivityLiveData.value?:false)
                    routesViewModel.getRoute(getRouteIdFromArguments(), connectivityLiveData.value?: false)
                } else {
                    binding.swipeRefreshLayout.isRefreshing = false
                }
            }
        }
    }

    private fun getData() {
        userProfileViewModel.currentUser(connectivityLiveData.value?: false)
        routesViewModel.getRoute(getRouteIdFromArguments(), connectivityLiveData.value?: false)

        routesViewModel.lastRoute.observe(viewLifecycleOwner) {
            Log.i("UserRatingView", "Route $it")
            routesViewModel.getUserRatings(getRouteIdFromArguments(),connectivityLiveData.value ?: false)
        }

        routesViewModel.acceptedUsers.observe(viewLifecycleOwner) {
            Log.i("UserRatingView", "Accepted user ratings size: ${it.size}, values $it")
            userProfileViewModel.getAcceptedUsersFromUserRatings(it, connectivityLiveData.value ?: false)
        }

        userProfileViewModel.acceptedUsers.observe(viewLifecycleOwner) {
            Log.i("UserRatingView", "Accepted users size: ${it.size}, values $it")
            val actualUser = userProfileViewModel.currentUserModel.value?.email ?: ""
            userForRateAdapter.setActualUser(actualUser)

            if(it.size == 1) {
                with(binding) {
                    tvTravelAlone.visibility = View.VISIBLE
                    btnSubmitAlone.visibility = View.VISIBLE
                    btnSubmitRatings.visibility = View.GONE
                }
            } else {
                val requests = routesViewModel.acceptedUsers.value as MutableList<UserRating>
                userForRateAdapter.addRequests(requests.filter{ r -> r.userEmail != actualUser } as MutableList<UserRating>,it.filter{ r -> r.email != actualUser })
            }
        }

        connectivityLiveData.observe(requireActivity()) { isNetwork ->
            if(!isNetwork) {
                binding.tvNoInternet.visibility = View.VISIBLE
                binding.btnSubmitRatings.setOnClickListener {
                    this@UserRatingFragment.createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
                }
                binding.btnSubmitAlone.setOnClickListener {
                    this@UserRatingFragment.createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
                }
            } else {
                binding.tvNoInternet.visibility = View.GONE
                binding.btnSubmitRatings.setOnClickListener {
                    if(connectivityLiveData.value == true) {
                        val userList = userProfileViewModel.acceptedUsers.value ?: listOf()
                        val actualUser = userProfileViewModel.currentUserModel.value?.email ?: ""
                        userList.forEach { user ->
                            if(user.email != actualUser) {
                                userProfileViewModel.updateRate(user.email, ratings[user.email] ?: 0F)
                                routesViewModel.updateUserRatingRate(getRouteIdFromArguments(), user.email, ratings[user.email] ?: 0F)
                            } else {
                                userProfileViewModel.updateIsInRoute(user.email, false, "")
                            }

                        }

                        activity.getUser()
                        activity.apply {
                            finishedRoute = false
                        }
                        activity.reloadCommuteItem()
                    } else {
                        createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
                    }
                }

                binding.btnSubmitAlone.setOnClickListener {
                    if(connectivityLiveData.value == true) {
                        val actualUser = userProfileViewModel.currentUserModel.value?.email ?: ""
                        userProfileViewModel.updateIsInRoute(actualUser, false, "")

                        activity.apply {
                            routeId = ""
                            isInRoute = false
                            finishedRoute = false
                        }

                        activity.reloadCommuteItem()
                    } else {
                        createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
                    }
                }
            }
        }

        routesViewModel.loading.observe(viewLifecycleOwner) {
            updateLoadingBar(it)
        }
    }

    override fun onOptionSelected(userRating: UserRating, rate: Float) {
        Log.i("UserRatingView", "Detected change on user ${userRating.userEmail} to rate $rate")
        ratings[userRating.userEmail] = rate
    }

    private fun updateLoadingBar(isLoading : Boolean) {
        with(binding) {
            if(isLoading) {
                pbLoading.visibility = View.VISIBLE
                llyButton.visibility = View.GONE
            } else {
                pbLoading.visibility = View.GONE
                llyButton.visibility = View.VISIBLE
                binding.swipeRefreshLayout.isRefreshing = false
            }
        }
    }

}