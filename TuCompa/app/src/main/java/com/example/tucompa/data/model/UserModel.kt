package com.example.tucompa.data.model

import com.google.firebase.Timestamp

data class UserModel(
    val email: String = "",
    val image: String = "",
    val lastRoute: Timestamp = Timestamp.now(),
    val name: String = "",
    val quantityRating: Int = 0,
    val streak: Int = 0,
    val totalRating: Double = 0.0,
    @field:JvmField
    val isInRoute : Boolean = false,
    val currentRoute : String = ""
)