package com.example.tucompa.ui.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tucompa.R
import com.example.tucompa.databinding.UserForRatingBinding
import com.example.tucompa.domain.User
import com.example.tucompa.domain.UserRating
import com.example.tucompa.ui.view.listeners.RatingListener
import com.squareup.picasso.Picasso

class UserForRateAdapter (private var requestList: MutableList<UserRating>, private var userList: List<User>, private var actualUserEmail : String, private val listener: RatingListener) :
    RecyclerView.Adapter<UserForRateAdapter.ViewHolder>(){

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = UserForRatingBinding.bind(view)

        fun setListener(userRating: UserRating) {
            binding.ratingBar.setOnRatingBarChangeListener { bar, fl, _ ->
                listener.onOptionSelected(userRating, fl)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_for_rating, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.i("AcceptedRequestsAdapter", "Printing position position $position")
        val user : User = userList[position]
        val userRating = requestList[position]

        Log.i("AcceptedRequestsAdapter", "Got rating ${userRating.userEmail} of user ${user.email} and actual user is $actualUserEmail")

        if(userRating.userEmail != actualUserEmail) {
            with(holder.binding) {
                profileName.text = user.name
                Picasso.get().load(user.image).into(holder.binding.profilePhoto)
            }
        } else {
            holder.binding.root.visibility = View.GONE
        }

        holder.setListener(userRating)
    }

    fun addRequests(newRequest:  MutableList<UserRating>, newUser: List<User>) {
        Log.i("UserForRate", "Updating data request: $newRequest; user: $newUser")
        requestList = newRequest
        userList = newUser
        Log.i("UserForRate", "Updated data request: $newRequest; user: $newUser")
        notifyDataSetChanged()
    }

    fun setActualUser(newActualUser : String) {
        actualUserEmail = newActualUser
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = requestList.size
}