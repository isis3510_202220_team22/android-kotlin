package com.example.tucompa.data.database.entitites

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.tucompa.data.model.RouteModel
import java.util.*

@Entity(tableName = "route")
data class RouteEntity (
    @PrimaryKey val id : String,
    @ColumnInfo(name = "departure_date") val departureDate: Date,
    @ColumnInfo(name = "destination_station_name") val destinationStationName: String,
    @ColumnInfo(name = "origin_station_name") val originStationName : String,
    @ColumnInfo(name = "user_email") val userEmail: String,
    @ColumnInfo(name = "is_finished") val isFinished : Boolean
)

fun RouteModel.toEntity() = RouteEntity(id, departureDate.toDate(), destinationStationName, originStationName,userEmail,isFinished)