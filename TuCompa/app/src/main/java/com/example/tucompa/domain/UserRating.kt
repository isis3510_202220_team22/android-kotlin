package com.example.tucompa.domain

import com.example.tucompa.data.database.entitites.UserRatingEntity
import com.example.tucompa.data.model.UserRatingModel

data class UserRating (
    val rate : Float,
    val pending : Boolean,
    val inadequate : Boolean,
    val userEmail: String,
)

fun UserRatingModel.toDomain() = UserRating(rate, pending, inadequate, userEmail)
fun UserRatingEntity.toDomain() = UserRating(rate, pending, inadequate, userEmail)