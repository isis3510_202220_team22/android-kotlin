package com.example.tucompa.data.model

import com.google.firebase.Timestamp

data class ReportModel(
    var dateTime : Timestamp = Timestamp.now(),
    var description : String = "",
    var type : String = ""
)