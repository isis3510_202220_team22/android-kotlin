package com.example.tucompa.domain

import com.example.tucompa.data.model.WagonModel

data class Wagon(
    val description: String = "",
    val id: String = "",
    val stationName: String = ""
)

fun WagonModel.toDomain() = Wagon(description, id, stationName)