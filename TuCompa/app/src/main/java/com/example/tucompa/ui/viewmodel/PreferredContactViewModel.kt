package com.example.tucompa.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tucompa.Constants
import com.example.tucompa.data.repository.PreferredContactRepository
import com.example.tucompa.domain.PreferredContact
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class PreferredContactViewModel @Inject constructor(private val preferredContactRepository: PreferredContactRepository): ViewModel() {

    private val createPreferredContactChannel = Channel<AllEvents>()
    val allCreatePreferredContactEventsFlow = createPreferredContactChannel.receiveAsFlow()

    private val allPreferredContacts = MutableLiveData<List<PreferredContact>>()
    val preferredContactsList get() = allPreferredContacts

    private val currentPreferredContact = MutableLiveData<PreferredContact>()
    val lastPreferredContact get() = currentPreferredContact

    private val isLoading = MutableLiveData<Boolean>(false)
    val loading get() = isLoading

    fun validateAndPostPreferredContact(name: String, number: String, image: String) = viewModelScope.launch {
        when {
            number.toString().length != 10 ->
                createPreferredContactChannel.send(AllEvents.Error(Constants.PHONE_NUMBER_LENGTH))
            name == "" || name == null->
                createPreferredContactChannel.send(AllEvents.Error(Constants.EMPTY_NAME))
            image == "" || image == null->
                createPreferredContactChannel.send(AllEvents.Error(Constants.EMPTY_IMAGE))
            else -> postPreferredContact(name, number, image)
        }
    }

    fun postPreferredContact(name: String, number: String, image: String) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            val preferredContact =
                preferredContactRepository.postPreferredContact(name, number, image)

            currentPreferredContact.postValue(preferredContact)
            createPreferredContactChannel.send(AllEvents.Message("PostPreferredContact: Succes"))
        }
    }

    fun getPreferredContacts() = viewModelScope.launch {
        withContext(Dispatchers.IO){
            val preferredContacts = preferredContactRepository.getPreferredContacts()

            allPreferredContacts.postValue(preferredContacts)
        }
    }

    fun getPreferredContact(number: String) = viewModelScope.launch {
        withContext(Dispatchers.IO){
            var preferredContact = preferredContactRepository.getPreferredContact(number)
            currentPreferredContact.postValue(preferredContact)
        }
    }

    fun deletePreferredContacts() = viewModelScope.launch {
        withContext(Dispatchers.IO){
            preferredContactRepository.deletePreferredContacts()
        }
    }

    sealed class AllEvents {
        data class Message(val message: String) : AllEvents()
        data class Error(val error: String) : AllEvents()
    }
}