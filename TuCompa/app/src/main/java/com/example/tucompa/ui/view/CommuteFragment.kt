package com.example.tucompa.ui.view

import android.app.AlertDialog
import android.content.DialogInterface

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.core.ActivityExtentions.createDialog
import com.example.tucompa.databinding.FragmentCommuteBinding
import com.example.tucompa.domain.UserRating
import com.example.tucompa.ui.view.adapters.AcceptedRequestAdapter
import com.example.tucompa.ui.view.adapters.PendingRequestAdapter
import com.example.tucompa.ui.view.listeners.CommuteListener
import com.example.tucompa.ui.viewmodel.RoutesViewModel
import com.example.tucompa.ui.viewmodel.UserProfileViewModel
import com.google.firebase.Timestamp
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import kotlin.math.min

@AndroidEntryPoint
class CommuteFragment : Fragment(), CommuteListener {

    private lateinit var binding : FragmentCommuteBinding
    private lateinit var activity: AppActivity

    private val userProfileViewModel : UserProfileViewModel by viewModels()
    private val routesViewModel : RoutesViewModel by viewModels()

    private lateinit var pendingRequestAdapter: PendingRequestAdapter
    private lateinit var acceptedRequestAdapter: AcceptedRequestAdapter

    private lateinit var connectivityLiveData: ConnectivityLiveData

    private var routeStarted : Boolean = false

    private var mLastAcceptClickTime = 0L
    private var mLastRejectClickTime = 0L

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentCommuteBinding.inflate(inflater , container , false)
        activity = getActivity() as AppActivity

        connectivityLiveData = ConnectivityLiveData(requireActivity())
        connectivityLiveData.checkValidNetworks()

        val bar: View? = activity.findViewById(R.id.lyBack) ?: null
        if (bar != null) {
            bar.visibility = View.VISIBLE
        }

        val btnBack: Button? = activity?.findViewById(R.id.btnBack) ?: null
        if (btnBack != null) {
            btnBack.visibility = View.GONE
        }

        val tvTop: TextView? = activity.findViewById(R.id.txtNavTittle) ?: null
        if (tvTop != null) {
            tvTop.text = "Información del Viaje"
        }

        getData()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pendingRequestAdapter = PendingRequestAdapter(mutableListOf(), mutableListOf(), this)
        binding.rvPending.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = pendingRequestAdapter
            Log.i("CommuteView", "Adapter set for rvPending")
        }

        acceptedRequestAdapter = AcceptedRequestAdapter(mutableListOf(), mutableListOf(), false,"","",this)
        binding.rvAccepted.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = acceptedRequestAdapter
            Log.i("CommuteView", "Adapter set for rvAccepted")
        }

        binding.btnDeleteCommute.setOnClickListener {
            if(connectivityLiveData.value == true) {
                val alertDialog = AlertDialog.Builder(activity)
                alertDialog.apply {
                    setTitle("Eliminar Viaje")
                    setMessage("¿Está seguro que desea eliminar el viaje?")
                        .setPositiveButton("Si",
                            DialogInterface.OnClickListener { dialog, id ->
                                userProfileViewModel.acceptedUsers.value?.forEach{ user ->
                                    userProfileViewModel.updateIsInRoute(user.email, false, "")
                                }
                                routesViewModel.deleteRoute(getRouteIdFromArguments())
                                activity.getUser()
                                activity.reloadCommuteItem()
                            })
                        .setNegativeButton("No",
                            DialogInterface.OnClickListener { dialog, id ->
                                dialog.dismiss()
                            })
                }.create().show()
            } else {
                this@CommuteFragment.createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
            }
        }

        binding.btnFinishCommute.setOnClickListener {
            if(connectivityLiveData.value == true) {
                val alertDialog = AlertDialog.Builder(activity)
                alertDialog.apply {
                    setTitle("Finalizar Viaje")
                    setMessage("¿Está seguro que desea finalizar el viaje?")
                        .setPositiveButton("Si",
                            DialogInterface.OnClickListener { dialog, id ->
                                routesViewModel.finishRoute(getRouteIdFromArguments())
                                activity.getActualRoute()
                                activity.getUser()
                                activity.reloadCommuteItem()

                            })
                        .setNegativeButton("No",
                            DialogInterface.OnClickListener { dialog, id ->
                                dialog.dismiss()
                            })
                }.create().show()
            } else {
                this@CommuteFragment.createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
            }
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            if(connectivityLiveData.value == true) {
                userProfileViewModel.currentUser(connectivityLiveData.value?:false)
                routesViewModel.getRoute(getRouteIdFromArguments(), connectivityLiveData.value ?: false)
            } else {
                binding.swipeRefreshLayout.isRefreshing = false
            }
        }
    }

    private fun getData() {
        userProfileViewModel.currentUser(connectivityLiveData.value?: false)
        routesViewModel.getRoute(getRouteIdFromArguments(), connectivityLiveData.value?: false)

        routesViewModel.lastRoute.observe(viewLifecycleOwner) {
            Log.i("Commute View", "Route $it")

            if(it.isFinished) {
                activity.finishedRoute = true
                activity.reloadCommuteItem()
            }

            var departureTimeStr = ""
            acceptedRequestAdapter.setAdminUser(it.userEmail)

            val calendar = Calendar.getInstance()
            calendar.time = it.departureDate.toDate()
            val departureMinute = calendar.get(Calendar.MINUTE)

            var minuteStr = "$departureMinute"

            if(departureMinute < 10) {
                minuteStr = "0$minuteStr"
            }

            departureTimeStr = "${calendar.get(Calendar.HOUR_OF_DAY)} : $minuteStr"

            with(binding) {
                tvDepartureTime.text = departureTimeStr
                tvDepartureStation.text = it.originStationName
                tvDestinationStation.text = it.destinationStationName
            }

            routesViewModel.getUserRatings(getRouteIdFromArguments(), connectivityLiveData.value ?: false)
        }

        routesViewModel.pendingUsers.observe(viewLifecycleOwner) {
            Log.i("CommuteView", "Pending user ratings size: ${it.size}")
            userProfileViewModel.getPendingUsersFromUserRatings(it, connectivityLiveData.value ?: false)
        }

        userProfileViewModel.pendingUsers.observe(viewLifecycleOwner) {
            Log.i("CommuteView", "Pending user size: ${it.size}")
            if(it.isNotEmpty()) {
                binding.tvNotPendingRequests.visibility = View.GONE

                val requests = routesViewModel.pendingUsers.value as MutableList<UserRating>
                pendingRequestAdapter.addRequests(requests,it)
            } else {
                pendingRequestAdapter.addRequests(mutableListOf(),it)
                binding.tvNotPendingRequests.visibility = View.VISIBLE
            }
        }

        routesViewModel.acceptedUsers.observe(viewLifecycleOwner) {
            Log.i("CommuteView", "Accepted user ratings size: ${it.size}, values $it")
            userProfileViewModel.getAcceptedUsersFromUserRatings(it, connectivityLiveData.value ?: false)
        }

        userProfileViewModel.acceptedUsers.observe(viewLifecycleOwner) {
            Log.i("CommuteView", "Accepted users size: ${it.size}, values $it")
            val actualUser = userProfileViewModel.currentUserModel.value?.email ?: ""
            acceptedRequestAdapter.setActualUser(actualUser)
            if(routesViewModel.lastRoute.value?.userEmail == userProfileViewModel.currentUserModel.value?.email) {
                acceptedRequestAdapter.setAdmin(true)
                routesViewModel.lastRoute.value?.departureDate?.let { it1 ->
                    activateDeleteOrFinish(
                        it1
                    )
                }
            } else {
                binding.llyPending.visibility = View.GONE
            }
            val requests = routesViewModel.acceptedUsers.value as MutableList<UserRating>
            acceptedRequestAdapter.addRequests(requests, it)
        }

        connectivityLiveData.observe(viewLifecycleOwner) { isNetwork ->
            if(!isNetwork) {
                binding.tvNoInternet.visibility = View.VISIBLE
                activity.showNoInternetSnackBar()
            } else {
                binding.tvNoInternet.visibility = View.GONE
            }
        }

        routesViewModel.loading.observe(viewLifecycleOwner) {
            updateLoadingBar(it)
        }
    }

    private fun activateDeleteOrFinish(departureDate : Timestamp) {
        validateRouteHasStarted(departureDate)

        if(routeStarted) {
            binding.llyPending.visibility = View.GONE

            binding.btnFinishCommute.visibility = View.VISIBLE
            binding.btnDeleteCommute.visibility = View.GONE
        } else {
            binding.btnFinishCommute.visibility = View.GONE
            binding.btnDeleteCommute.visibility = View.VISIBLE
        }

    }

    private fun validateRouteHasStarted(routeStartedTime : Timestamp) {
        val routeDate = routeStartedTime.toDate()
        Log.i("CommuteView", "Comparing $routeStartedTime with now")
        if(routeDate.compareTo(Date()) == -1) {
            routeStarted = true
        }
    }

    private fun getRouteIdFromArguments() : String {
        val routeId = this.arguments?.get("routeId").toString()
        return routeId
    }

    override fun onAcceptButton(userRating: UserRating) {
        connectivityLiveData.checkValidNetworks()
        userProfileViewModel.getUser(userRating.userEmail, connectivityLiveData.value ?: false)

        if(!acceptedIsClickRecently()) {
            connectivityLiveData.value?.let { connectivity ->
                if(connectivity) {
                    userProfileViewModel.lastFetchedUser.value?.let {
                        Log.i("CommuteView", "Fetched user ${it.email}, is in route ${it.isInRoute}")
                        if (it.isInRoute) {
                            this@CommuteFragment.createDialog("Espera!", "Parece que este usuario ya está en una ruta")
                            routesViewModel.deleteUserRating(getRouteIdFromArguments(), it.email)
                        } else {
                            routesViewModel.acceptUserRating(getRouteIdFromArguments(), userRating.userEmail)
                            userProfileViewModel.updateIsInRoute(userRating.userEmail, true, getRouteIdFromArguments())
                        }
                    }
                } else {
                    this@CommuteFragment.createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
                }
            }
        }

    }

    override fun onDeclineButton(userRating: UserRating) {
        connectivityLiveData.checkValidNetworks()

        if(!rejectIsClickRecently()) {
            connectivityLiveData.value?.let {
                if(it) {
                    routesViewModel.deleteUserRating(getRouteIdFromArguments(), userRating.userEmail)
                } else {
                    this@CommuteFragment.createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
                }
            }
        }
    }

    override fun onMessageButton(user: UserRating) {
        this.createDialog("Espera", "Esta función aún no está disponible")
    }

    override fun onDeleteButton(userRating: UserRating) {
        connectivityLiveData.checkValidNetworks()

        connectivityLiveData.value?.let {
            if(it) {
                val alertDialog = AlertDialog.Builder(activity)
                alertDialog.apply {
                    setTitle("Eliminar usuario")
                    setMessage("¿Está seguro que desea eliminar al usuario?")
                        .setPositiveButton("Si",
                            DialogInterface.OnClickListener { dialog, id ->
                                val userEmail = userRating.userEmail

                                userProfileViewModel.updateIsInRoute(userEmail, false, "")
                                routesViewModel.deleteUserRating(getRouteIdFromArguments(), userEmail)
                            })
                        .setNegativeButton("No",
                            DialogInterface.OnClickListener { dialog, id ->
                                dialog.dismiss()
                            })
                }.create().show()
            } else {
                this@CommuteFragment.createDialog("Sin internet", Constants.LOGIN_CONNECTION_ERROR)
            }
        }
    }

    private fun updateLoadingBar(isLoading : Boolean) {
        with(binding) {
            if(isLoading) {
                pbLoading.visibility = View.VISIBLE
                llyButton.visibility = View.GONE
            } else {
                pbLoading.visibility = View.GONE
                llyButton.visibility = View.VISIBLE
                binding.swipeRefreshLayout.isRefreshing = false
            }
        }
    }

    fun acceptedIsClickRecently(): Boolean {
        if (SystemClock.elapsedRealtime() - mLastAcceptClickTime < 1000) {
            return true
        }
        mLastAcceptClickTime = SystemClock.elapsedRealtime()
        return false
    }

    fun rejectIsClickRecently(): Boolean {
        if (SystemClock.elapsedRealtime() - mLastRejectClickTime < 1000) {
            return true
        }
        mLastRejectClickTime = SystemClock.elapsedRealtime()
        return false
    }
}