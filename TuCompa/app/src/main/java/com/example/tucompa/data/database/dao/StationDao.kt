package com.example.tucompa.data.database.dao

import androidx.room.*
import com.example.tucompa.data.database.entitites.StationEntity

@Dao
interface StationDao {
    @Query("SELECT * FROM station")
    suspend fun getAll(): List<StationEntity>

    @Query("SELECT * FROM station WHERE name = :name")
    suspend fun getUserById(name: String): StationEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg stations: StationEntity)

    @Delete
    suspend fun delete(route: StationEntity)
}