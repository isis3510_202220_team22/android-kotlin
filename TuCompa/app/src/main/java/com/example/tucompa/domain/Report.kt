package com.example.tucompa.domain

import com.example.tucompa.data.model.ReportModel
import com.google.firebase.Timestamp

data class Report(
    val dateTime : Timestamp,
    val description : String,
    val type : String
)

fun ReportModel.toDomain() = Report(dateTime, description, type)
