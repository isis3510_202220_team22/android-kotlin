package com.example.tucompa.data.repository

import com.example.tucompa.data.database.AppDatabase
import com.example.tucompa.data.database.entitites.toEntity
import com.example.tucompa.data.model.StationModel
import com.example.tucompa.data.network.StationService
import com.example.tucompa.domain.Station
import com.example.tucompa.domain.toDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class StationRepository @Inject constructor(
    private val stationService: StationService,
    private val appDatabase: AppDatabase
) {

    private val stationDao = appDatabase.stationDao()

    suspend fun getStations(isConnection: Boolean): List<Station> {
        return withContext(Dispatchers.IO){
            if (isConnection) {
                var stations: List<StationModel>? = stationService.getStations()

                if (!stations.isNullOrEmpty()) {
                    stations.forEach {
                        stationDao.insertAll(it.toEntity())
                    }
                    stations.map { station -> station.toDomain() }
                } else {
                    emptyList()
                }
            } else {
                val dbResponse = stationDao.getAll()

                if (!dbResponse.isNullOrEmpty()) {
                    dbResponse.map { station -> station.toDomain() }
                } else {
                    emptyList()
                }
            }
        }

    }
}