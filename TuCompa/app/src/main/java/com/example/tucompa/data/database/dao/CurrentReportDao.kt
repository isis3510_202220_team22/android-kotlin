package com.example.tucompa.data.database.dao

import androidx.room.*
import com.example.tucompa.data.database.entitites.CurrentReportEntity

@Dao
interface CurrentReportDao {
    @Query("SELECT * FROM current_report")
    suspend fun getAll(): List<CurrentReportEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg reports: CurrentReportEntity)

    @Query("DELETE FROM current_report")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(report: CurrentReportEntity)
}