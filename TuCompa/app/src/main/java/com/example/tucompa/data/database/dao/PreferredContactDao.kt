package com.example.tucompa.data.database.dao

import androidx.room.*
import com.example.tucompa.data.database.entitites.PreferredContactEntity

@Dao
interface PreferredContactDao {
    @Query("SELECT * FROM preferred_contact")
    suspend fun getAll(): List<PreferredContactEntity>

    @Query("SELECT * FROM preferred_contact WHERE number = :number")
    suspend fun getContactByPhoneNumber(number: String): PreferredContactEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg preferredContacts: PreferredContactEntity)

    @Query("DELETE FROM preferred_contact")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(preferredContact: PreferredContactEntity)
}