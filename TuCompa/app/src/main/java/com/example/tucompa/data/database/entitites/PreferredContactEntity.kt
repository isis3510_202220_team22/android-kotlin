package com.example.tucompa.data.database.entitites

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.tucompa.data.model.PreferredContactModel

@Entity(tableName = "preferred_contact")
data class PreferredContactEntity(
    @ColumnInfo(name = "name") val name: String,
    @PrimaryKey val number: String,
    @ColumnInfo(name = "image") val image: String,
)

fun PreferredContactModel.toEntity() = PreferredContactEntity(name, number, image)