package com.example.tucompa.data.network

import android.util.Log
import com.example.tucompa.Constants
import com.example.tucompa.data.model.WagonModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class WagonService @Inject constructor(private val db: FirebaseFirestore) {

    fun getWagon(name: String, id: String): WagonModel? {
        val collectionWagons = db.collection(Constants.STATION_COLLECTION_NAME).document(name)
            .collection(Constants.WAGON_COLLECTION_NAME).document(id)
        var currentWagon: WagonModel? = null

        val job = GlobalScope.launch(Dispatchers.IO) {
            currentWagon = collectionWagons.get().await().toObject(WagonModel::class.java)
            Log.d("WagonService", "DocumentSnapshot data : ${currentWagon}")
        }

        runBlocking {
            job.join()
        }
        return currentWagon
    }
}