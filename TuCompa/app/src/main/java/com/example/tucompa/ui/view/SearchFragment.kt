package com.example.tucompa.ui.view

import android.app.AlertDialog
import android.content.ContentValues
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tu.StationViewModel
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.core.ActivityExtentions.createDialog
import com.example.tucompa.databinding.FragmentSearchBinding
import com.example.tucompa.domain.Route
import com.example.tucompa.domain.User
import com.example.tucompa.domain.UserRating
import com.example.tucompa.ui.view.adapters.RouteAdapter
import com.example.tucompa.ui.view.listeners.CreateRatingListener
import com.example.tucompa.ui.viewmodel.RoutesViewModel
import com.example.tucompa.ui.viewmodel.UserProfileViewModel
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.google.firebase.Timestamp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.util.*

@AndroidEntryPoint
class SearchFragment : Fragment(R.layout.fragment_search), CreateRatingListener {

    private lateinit var binding: FragmentSearchBinding
    private var activity: AppActivity? = null

    private val userProfileViewModel: UserProfileViewModel by viewModels()
    private val routesViewModel: RoutesViewModel by viewModels()
    private val stationViewModel: StationViewModel by viewModels()

    private lateinit var routeAdapter: RouteAdapter

    private lateinit var connectivityLiveData: ConnectivityLiveData

    private var dateTime: Timestamp = Timestamp.now()
    private var departureStation1: String = ""
    private var destinationStation1: String = ""

    private var mLastClickTime = 0L

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchBinding.inflate(inflater, container, false)

        activity = getActivity() as AppActivity

        connectivityLiveData = context?.let { ConnectivityLiveData(it) }!!

        val bar: View? = activity?.findViewById(R.id.lyBack) ?: null
        if (bar != null) {
            bar.visibility = View.GONE
        }

        lifecycleScope.launch {
            routesViewModel.allGetRouteEventsFlow.collect { event ->
                when (event) {
                    is RoutesViewModel.AllEvents.Error -> {
                        when (event.error) {
                            Constants.PAST_DEPARTURE_TIME_ERROR -> {
                                clearErrors()
                                createDialog(
                                    "Error", Constants
                                        .CREATE_COMMUTE_ERRORS_MESSAGES[Constants.PAST_DEPARTURE_TIME_ERROR].toString()
                                )
                                binding.searchtop.requestFocus()
                            }
                            Constants.NON_EXISTENT_ORIGIN_STATION_ERROR -> {
                                clearErrors()
                                binding.departureStation.error =
                                    Constants.CREATE_COMMUTE_ERRORS_MESSAGES[
                                            Constants.NON_EXISTENT_ORIGIN_STATION_ERROR
                                    ]
                                binding.departureStation.requestFocus()
                            }
                            Constants.NON_EXISTENT_DESTINATION_STATION_ERROR -> {
                                clearErrors()
                                binding.destinationStation.error =
                                    Constants.CREATE_COMMUTE_ERRORS_MESSAGES[
                                            Constants.NON_EXISTENT_DESTINATION_STATION_ERROR
                                    ]
                                binding.destinationStation.requestFocus()
                            }
                            Constants.SAME_STATION_ERROR -> {
                                clearErrors()
                                binding.departureStation.error =
                                    Constants.CREATE_COMMUTE_ERRORS_MESSAGES[
                                            Constants.SAME_STATION_ERROR
                                    ]
                                binding.destinationStation.error =
                                    Constants.CREATE_COMMUTE_ERRORS_MESSAGES[
                                            Constants.SAME_STATION_ERROR
                                    ]
                                binding.departureStation.requestFocus()
                            }
                        }
                    }
                    is RoutesViewModel.AllEvents.Message -> {
                        clearErrors()
                    }
                    else -> {
                        Log.d(ContentValues.TAG, "listenToChannels: No event received so far")
                    }
                }
            }
        }

        userProfileViewModel.currentUser(connectivityLiveData.value?:false)

        //getStationNames()
        addObservers()
        setup()
        initRecyclerView()

        return binding.root
    }

    private fun initRecyclerView() {
        val recyclerView = binding.recyclerroute
        val manager = LinearLayoutManager(activity)

        recyclerView.layoutManager = manager

        routeAdapter = RouteAdapter(
            mutableListOf(),
            mutableListOf(),
            User(),
            mutableListOf<List<UserRating>>(),
            this
        )
        recyclerView.adapter = routeAdapter
    }

    private fun setup() {

        binding.apply {
            val timeNow = Calendar.getInstance()
            val timePicker =
                MaterialTimePicker.Builder()
                    .setTimeFormat(TimeFormat.CLOCK_12H)
                    .setHour(timeNow.get(Calendar.HOUR_OF_DAY))
                    .setMinute(timeNow.get(Calendar.MINUTE))
                    .setTitleText("Selecciona una hora")
                    .build()

            hourtext.setText(
                "${checkDigit(timeNow.get(Calendar.HOUR_OF_DAY))}:${checkDigit(timeNow.get(Calendar.MINUTE))}"
            )

            btnHour.setOnClickListener {
                if (!isClickRecently()) {
                    timePicker.show(requireActivity().supportFragmentManager, "MATERIAL_DATE_PICKER")
                }
            }

            timePicker.addOnPositiveButtonClickListener {
                hourtext.setText("${checkDigit(timePicker.hour)}:${checkDigit(timePicker.minute)}")
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.HOUR_OF_DAY, timePicker.hour)
                calendar.set(Calendar.MINUTE, timePicker.minute)
                calendar.set(Calendar.SECOND, 0)

                dateTime = Timestamp(Date(calendar.timeInMillis))
            }

            connectivityLiveData.checkValidNetworks()

            connectivityLiveData.observe(viewLifecycleOwner) { isNetwork ->

                stationViewModel.getStationNames(isNetwork)

                swipeRefreshLayout.setOnRefreshListener {
                    clearErrors()

                    if (isNetwork) {
                        destinationStation1 = actvDestinationStation.text.toString().trim()
                        departureStation1 = actvDepartureStation.text.toString().trim()

                        if (timePicker != null) {

                            routesViewModel.validateAndGetRoutesConditional(
                                dateTime,
                                destinationStation1,
                                departureStation1,
                                isNetwork
                            )

                        } else {

                            routesViewModel.validateAndGetRoutesConditional(
                                null,
                                destinationStation1,
                                departureStation1,
                                isNetwork
                            )

                        }

                        routeAdapter.notifyDataSetChanged()
                    } else {
                        swipeRefreshLayout.isRefreshing = false
                        openDialog(Constants.SEARCH_CONNECTION_ERROR)
                    }
                }

                btnSearchCommute.setOnClickListener {
                    clearErrors()

                    if (isNetwork) {
                        destinationStation1 = actvDestinationStation.text.toString().trim()
                        departureStation1 = actvDepartureStation.text.toString().trim()

                        if (timePicker != null) {

                            routesViewModel.validateAndGetRoutesConditional(
                                dateTime,
                                destinationStation1,
                                departureStation1,
                                isNetwork
                            )

                        } else {

                            routesViewModel.validateAndGetRoutesConditional(
                                null,
                                destinationStation1,
                                departureStation1,
                                isNetwork
                            )

                        }

                        routeAdapter.notifyDataSetChanged()
                    } else {
                        openDialog(Constants.SEARCH_CONNECTION_ERROR)
                    }
                }

            }

        }
    }

    private fun addObservers() {

        stationViewModel.stationNames.observe(viewLifecycleOwner) {
            val adapter = this.activity?.let { it1 ->
                ArrayAdapter(
                    it1, android.R.layout.simple_list_item_1,
                    it
                )
            }
            binding.actvDepartureStation.setAdapter(adapter)
            binding.actvDestinationStation.setAdapter(adapter)
        }

        routesViewModel.allRoutesConditional.observe(viewLifecycleOwner) {
            if (it != null) {
                if (it.isEmpty()) {
                    binding.noroutesfound.visibility = View.VISIBLE
                    routeAdapter.setActualRouteAndUserList(mutableListOf<Route>(), mutableListOf<User>())
                } else {
                    binding.noroutesfound.visibility = View.GONE
                    userProfileViewModel.getUsersFromRouteList(it, connectivityLiveData.value ?: false)
                    routesViewModel.getUserRatingsListFromRoutesList(it,connectivityLiveData.value ?: false)
                }
            }
        }

        userProfileViewModel.routesAndUsers.observe(viewLifecycleOwner) {
            if (it != null) {
                routesViewModel.allRoutesConditional.value?.let { it1 ->
                    routeAdapter.setActualRouteAndUserList(it1, it)
                }
            }
        }

        userProfileViewModel.currentUserModel.observe(viewLifecycleOwner) {
            if (it != null) {
                routeAdapter.setActualUser(it)
            }
        }

        routesViewModel.allUsersRatingsFound.observe(viewLifecycleOwner) {
            if (it != null) {
                routeAdapter.setActualUserRatingsAllRoutes(it)
            }
        }

        routesViewModel.searching.observe(viewLifecycleOwner) {
            updateLoadingBar(it)
        }

    }

    private fun clearErrors() {
        with(binding) {
            departureStation.error = null
            destinationStation.error = null
        }
    }

    override fun onCreateRating(
        routeId: String,
        pending: Boolean,
        userEmail: String
    ) {
        routesViewModel.createUserRating(routeId, pending, userEmail)

        connectivityLiveData.checkValidNetworks()

            routesViewModel.validateAndGetRoutesConditional(
                dateTime,
                destinationStation1,
                departureStation1,
                connectivityLiveData.value?:false
            )

    }

    override fun onGoingToCommute(routeId: String) {
        activity?.getUser()
        activity?.getActualRoute()
        activity?.reloadCommuteItem()
    }

    private fun checkDigit(number: Int): String {
        var response = ""
        if (number <= 9) {
            response = "0${number}"
        } else {
            response = "${number}"
        }
        return response
    }

    private fun openDialog(body: String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Sin conexion").setMessage(body)
            .setPositiveButton("OK") { _, _ -> }
        val dialogo = builder.create()
        dialogo.show()
    }

    private fun updateLoadingBar(isLoading : Boolean) {
        with(binding){
            if(isLoading){
                pbLoading.visibility = View.VISIBLE
            } else {
                pbLoading.visibility = View.GONE
                binding.swipeRefreshLayout.isRefreshing = false;
            }
        }
    }

    fun isClickRecently(): Boolean {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return true
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        return false
    }

}