package com.example.tucompa.ui.view

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.os.Bundle
import android.os.Trace
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.tu.StationViewModel
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.core.ActivityExtentions.createDialog
import com.example.tucompa.databinding.FragmentCreateCommuteBinding
import com.example.tucompa.ui.viewmodel.ReportViewModel
import com.example.tucompa.ui.viewmodel.RoutesViewModel
import com.example.tucompa.ui.viewmodel.UserProfileViewModel
import com.example.tucompa.ui.viewmodel.UserRegistrationViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.Timestamp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.util.*

@AndroidEntryPoint
class CreateCommuteFragment : Fragment(R.layout.fragment_create_commute) {

    private lateinit var binding : FragmentCreateCommuteBinding
    private var activity: AppActivity? = null

    private val userRegistrationViewModel : UserRegistrationViewModel by viewModels()
    private val userProfileViewModel : UserProfileViewModel by viewModels()
    private val stationViewModel : StationViewModel by viewModels()
    private val routesViewModel : RoutesViewModel by viewModels()
    private val reportViewModel : ReportViewModel by viewModels()

    private lateinit var connectivityLiveData: ConnectivityLiveData

    private var departureDate : Timestamp = Timestamp.now()
    private var originStation : String = ""
    private var destinationStation : String = ""
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCreateCommuteBinding.inflate(inflater , container , false)

        activity = getActivity() as AppActivity
        connectivityLiveData = ConnectivityLiveData(requireActivity())
        connectivityLiveData.checkValidNetworks()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())

        val bar: View? = activity?.findViewById(R.id.lyBack) ?: null
        if (bar != null) {
            bar.visibility = View.VISIBLE
        }

        val tvTop: TextView? = activity?.findViewById(R.id.txtNavTittle) ?: null
        if (tvTop != null) {
            tvTop.text = "Publicar Viaje"
        }

        val btnBack: Button? = activity?.findViewById(R.id.btnBack) ?: null
        if (btnBack != null) {
            btnBack.visibility = View.GONE
        }

        lifecycleScope.launch {
            routesViewModel.allCreateRouteEventsFlow.collect{ event ->
                when(event) {
                    is RoutesViewModel.AllEvents.Error -> {
                        when(event.error) {
                            Constants.PAST_DEPARTURE_TIME_ERROR -> {
                                clearErrors()
                                createDialog("Error",Constants
                                    .CREATE_COMMUTE_ERRORS_MESSAGES[Constants.PAST_DEPARTURE_TIME_ERROR].toString())
                                binding.lyPublishComuteMiddle.requestFocus()
                            }
                            Constants.NON_EXISTENT_ORIGIN_STATION_ERROR -> {
                                clearErrors()
                                binding.tilDepartureStation.error = Constants.CREATE_COMMUTE_ERRORS_MESSAGES[
                                        Constants.NON_EXISTENT_ORIGIN_STATION_ERROR
                                ]
                                binding.tilDepartureStation.requestFocus()
                            }
                            Constants.NON_EXISTENT_DESTINATION_STATION_ERROR -> {
                                clearErrors()
                                binding.tilDestinationStation.error = Constants.CREATE_COMMUTE_ERRORS_MESSAGES[
                                        Constants.NON_EXISTENT_DESTINATION_STATION_ERROR
                                ]
                                binding.tilDestinationStation.requestFocus()
                            }
                            Constants.SAME_STATION_ERROR -> {
                                clearErrors()
                                binding.tilDepartureStation.error = Constants.CREATE_COMMUTE_ERRORS_MESSAGES[
                                        Constants.SAME_STATION_ERROR
                                ]
                                binding.tilDestinationStation.error = Constants.CREATE_COMMUTE_ERRORS_MESSAGES[
                                        Constants.SAME_STATION_ERROR
                                ]
                                binding.tilDepartureStation.requestFocus()
                            }
                        }
                    }
                    is RoutesViewModel.AllEvents.Message -> {
                        clearErrors()
                        val routeId = routesViewModel.lastRoute.value!!.id

                        userProfileViewModel.updateLoggedUserIsInRoute(true, routeId)
                        reportViewModel.validateAndGetReportsFromLastHourByStation(originStation, destinationStation)
                        userProfileViewModel.updateLastRouteAndStreak(Timestamp.now())
                        createToast("Tu ruta ha sido publicada!")
                    }
                    else ->{
                        Log.d(ContentValues.TAG, "listenToChannels: No event received so far")
                    }
                }
            }
        }
        getUser()
        addObservers()
        stationViewModel.getStationNames(connectivityLiveData.value?:false)
        getLastKnownLocation()
        setup()
        return binding.root
    }

    private fun setup() {

        binding.apply {
            timePicker.setOnTimeChangedListener(TimePicker.OnTimeChangedListener { _, hour, minute ->
                val calendar = Calendar.getInstance()
                calendar.set(Calendar.HOUR_OF_DAY, hour)
                calendar.set(Calendar.MINUTE, minute)
                calendar.set(Calendar.SECOND, 0)

                departureDate = Timestamp(Date(calendar.timeInMillis))
            })

            btnCreateCommute.setOnClickListener {
                Trace.beginSection("CreateCommuteTrace")
                originStation = actvDepartureStation.text.toString().trim()
                destinationStation = actvDestinationStation.text.toString().trim()
                val userEmail = userRegistrationViewModel.currentUser.value?.email.toString()


                routesViewModel.validateAndCreateRoute(departureDate, destinationStation, originStation, userEmail, connectivityLiveData.value?:false)
                Trace.endSection()
            }
        }
    }

    private fun addObservers() {

        routesViewModel.lastRoute.observe(viewLifecycleOwner) {
            activity?.routeId = it.id

            activity?.isInRoute = it.id != ""

            activity?.reloadCommuteItem()
        }

        stationViewModel.stationNames.observe(viewLifecycleOwner) {
            val adapter = this.activity?.let { it1 ->
                ArrayAdapter(
                    it1, android.R.layout.simple_list_item_1,
                    it)
            }
            binding.actvDepartureStation.setAdapter(adapter)
            binding.actvDestinationStation.setAdapter(adapter)
        }

        reportViewModel.qReports.observe(viewLifecycleOwner) {
            val numberOfReports = it.values.first().toInt()
            val stationName = it.keys.first()
            val numberOfReports2 = it.values.last().toInt()
            val stationName2 = it.keys.last()
            var message = ""
            if(numberOfReports >= Constants.MINIMUM_REPORTS_FOR_ALERT) {
                message = "En la estación $stationName se han registrado $numberOfReports reportes" +
                        " el dia de hoy.\n"
            }
            if(numberOfReports2 >= Constants.MINIMUM_REPORTS_FOR_ALERT) {
                if(!message.equals("")) {
                    message += "Además, en"
                }
                else {
                    message += "En"
                }
                message += " la estación $stationName2 se han registrado $numberOfReports2 reportes" +
                        " el dia de hoy."
            }

            if(!message.equals("")) {
                showDefaultDialog("Tenga cuidado: \n" + message)
            }
        }

        connectivityLiveData.checkValidNetworks()

        connectivityLiveData.observe(requireActivity()) { isNetwork ->

            stationViewModel.getStationNames(isNetwork)

            binding.btnCreateCommute.setOnClickListener {
                originStation = binding.actvDepartureStation.text.toString().trim()
                destinationStation = binding.actvDestinationStation.text.toString().trim()
                val userEmail = userRegistrationViewModel.currentUser.value?.email.toString()

                routesViewModel.validateAndCreateRoute(departureDate, destinationStation, originStation, userEmail, isNetwork)
            }

            binding.btnCreateCommute.isEnabled = isNetwork

            if(!isNetwork) {
                binding.tvNoInternet.visibility = View.VISIBLE
            } else {
                binding.tvNoInternet.visibility = View.GONE
            }
        }

        routesViewModel.loading.observe(viewLifecycleOwner) { isLoading ->
           updateLoadingBar(isLoading)
        }

        stationViewModel.frequentStations.observe(viewLifecycleOwner) {
            Log.i("CreateCommuteView", "Got frequent station data $it")
            if(it.isNotEmpty() && connectivityLiveData.value == true) {

                val originStation = it[0]
                val destinationStation = it[1]

                if(!originStation.isNullOrEmpty() && (originStation != activity?.lastStationSugested || activity?.lastTimeSuggested == -1L || Date().time - (activity?.lastTimeSuggested ?: 0L) >= MILLISECONDS_IN_AN_HOUR)) {
                    var btnMessage = "Si, quiero ese origen"
                    var message = "De acuerdo con su ubicación, le sugerimos empezar su ruta en la siguiente estación: $originStation"
                    if(!destinationStation.isNullOrEmpty()) {
                        btnMessage = "Si, quiero esa ruta"
                        message = "De acuerdo con su ubicación y sus rutas frecuentes le sugerimos la siguiente ruta: $originStation - $destinationStation"
                    }
                    val alertDialog = AlertDialog.Builder(activity)
                    alertDialog.apply {
                        setTitle("Ruta sugerida")
                        setMessage(message)
                            .setPositiveButton(btnMessage,
                                DialogInterface.OnClickListener { dialog, id ->
                                    with(binding) {
                                        if(!originStation.isNullOrEmpty()) {actvDepartureStation.setText(originStation)}
                                        if(!destinationStation.isNullOrEmpty()) {actvDestinationStation.setText(destinationStation)}
                                        stationViewModel.getStationNames(connectivityLiveData.value?:false)
                                    }
                                    routesViewModel.logRecommendedRouteEvent(userRegistrationViewModel.currentUser.value?.email ?: "")
                                })
                            .setNegativeButton("No, gracias.",
                                DialogInterface.OnClickListener { dialog, id ->
                                    dialog.dismiss()
                                })
                    }.create().show()
                    if(connectivityLiveData.value == true) {
                        routesViewModel.logCommuteViewOpened(userRegistrationViewModel.currentUser.value?.email ?: "")
                    }

                    activity?.lastStationSugested = originStation
                    activity?.lastTimeSuggested = Date().time

                    Log.i("CreateCommuteView", "Updated data:LTS: ${activity?.lastTimeSuggested}")

                }
            }
        }
    }

    fun getLastKnownLocation() {
        Log.i("CreateCommuteView", "Trying to read user location")
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location->
                Log.i("CreateCommuteView", "Got location $location")
                if (location != null && connectivityLiveData.value == true) {
                    val actualUser = userRegistrationViewModel.currentUser.value
                    if(actualUser != null) {
                        actualUser.email?.let { stationViewModel.getMostFrequentRoute(it, location.latitude, location.longitude) }
                    }
                }

            }.addOnFailureListener {
                Log.i("CreateCommuteView", "Failed to read user location $it")
            }
    }

    private fun showDefaultDialog(message: String) {
        val alertDialog = AlertDialog.Builder(activity)
        alertDialog.apply {
            setTitle("Reportes")
            setMessage(message).setPositiveButton("Ok",
                DialogInterface.OnClickListener { dialog, id ->
                    dialog.dismiss()
                })
        }.create().show()
    }

    private fun clearErrors() {
        with(binding) {
            tilDepartureStation.error = null
            tilDestinationStation.error = null
        }
    }

    private fun getUser() {
        userRegistrationViewModel.getCurrentUser()
    }

    private fun createToast(message : String) {
        val toast = Toast.makeText(activity, message, Toast.LENGTH_SHORT)
        toast.show()
    }

    private fun updateLoadingBar(isLoading : Boolean) {
        with(binding) {
            if(isLoading) {
                pbLoading.visibility = View.VISIBLE
                btnCreateCommute.visibility = View.GONE
            } else {
                pbLoading.visibility = View.GONE
                btnCreateCommute.visibility = View.VISIBLE
            }
        }
    }

    companion object {
        const val MILLISECONDS_IN_AN_HOUR = 60000 // 7200000
    }
}