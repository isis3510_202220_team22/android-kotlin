package com.example.tucompa.ui.view

import android.app.AlertDialog
import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.core.ActivityExtentions.createDialog
import com.example.tucompa.databinding.FragmentAddPreferredContactBinding
import com.example.tucompa.ui.viewmodel.PreferredContactViewModel
import com.example.tucompa.ui.viewmodel.ReportViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class AddPreferredContactFragment : Fragment(R.layout.fragment_add_preferred_contact) {

    private lateinit var binding: FragmentAddPreferredContactBinding
    private var activity: AppActivity? = null

    private val preferredContactViewModel: PreferredContactViewModel by viewModels()

    private lateinit var connectivityLiveData: ConnectivityLiveData

    private var contactName1: String = ""
    private var contactNumber1: String = ""
    private var contactImage1: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddPreferredContactBinding.inflate(inflater, container, false)

        activity = getActivity() as AppActivity

        connectivityLiveData = context?.let { ConnectivityLiveData(it) }!!

        val bar: View? = activity?.findViewById(R.id.lyBack) ?: null
        if (bar != null) {
            bar.visibility = View.VISIBLE
        }

        val tvTop: TextView? = activity?.findViewById(R.id.txtNavTittle) ?: null
        if (tvTop != null) {
            tvTop.text = "Agregar contacto favorito"
        }

        lifecycleScope.launch {
            preferredContactViewModel.allCreatePreferredContactEventsFlow.collect { event ->
                when(event) {
                    is PreferredContactViewModel.AllEvents.Error -> {
                        when(event.error) {
                            Constants.PHONE_NUMBER_LENGTH -> {
                                clearErrors()
                                binding.contactNumber.error = Constants.PHONE_NUMBER_LENGTH
                                binding.contactNumber.requestFocus()
                            }
                            Constants.EMPTY_NAME -> {
                                clearErrors()
                                binding.contactName.error = Constants.EMPTY_NAME
                                binding.contactName.requestFocus()
                            }
                            Constants.EMPTY_IMAGE -> {
                                clearErrors()
                                createDialog("Foto del contacto", Constants.EMPTY_IMAGE)
                            }
                        }
                    }
                    is PreferredContactViewModel.AllEvents.Message -> {
                        clearErrors()
                        openDialog()
                        binding.name.setText("")
                        binding.number.setText("")
                        imageSize(4)
                    }
                    else -> {
                        Log.d(ContentValues.TAG, "listenToChannels: No event received so far")
                    }
                }
            }
        }

        setup()

        return binding.root
    }

    private fun setup() {
        binding.apply {
            connectivityLiveData.checkValidNetworks()

            connectivityLiveData.observe(viewLifecycleOwner) { isNetwork ->
                if (!isNetwork){
                    openDialog(Constants.ADD_PREFERRED_CONTACT_CONNECTION_ERROR)
                } else {
                    Picasso.get().load("https://cdn-icons-png.flaticon.com/512/4441/4441180.png").into(family)
                    Picasso.get().load("https://cdn-icons-png.flaticon.com/512/2024/2024101.png").into(friend)
                    Picasso.get().load("https://cdn-icons-png.flaticon.com/512/3074/3074318.png").into(significantOther)
                }
            }

            familyPhoto.setOnClickListener {
                contactImage1 = "https://cdn-icons-png.flaticon.com/512/4441/4441180.png"
                imageSize(1)
            }

            friendPhoto.setOnClickListener {
                contactImage1 = "https://cdn-icons-png.flaticon.com/512/2024/2024101.png"
                imageSize(2)
            }

            significantOtherPhoto.setOnClickListener {
                contactImage1 = "https://cdn-icons-png.flaticon.com/512/3074/3074318.png"
                imageSize(3)
            }

            btnAddPreferredContact.setOnClickListener {
                contactNumber1 = number.text.toString()

                contactName1 = name.text.toString()

                preferredContactViewModel.validateAndPostPreferredContact(contactName1, contactNumber1, contactImage1)
            }
        }
    }

    private fun imageSize(imgNumber: Int){
        when(imgNumber){
            1 -> {
                binding.family.layoutParams.height = 200
                binding.family.layoutParams.width = 200
                binding.family.requestLayout()

                binding.friend.layoutParams.height = 160
                binding.friend.layoutParams.width = 160
                binding.friend.requestLayout()

                binding.significantOther.layoutParams.height = 160
                binding.significantOther.layoutParams.width = 160
                binding.significantOther.requestLayout()
            }
            2 -> {
                binding.family.layoutParams.height = 160
                binding.family.layoutParams.width = 160
                binding.family.requestLayout()

                binding.friend.layoutParams.height = 200
                binding.friend.layoutParams.width = 200
                binding.friend.requestLayout()

                binding.significantOther.layoutParams.height = 160
                binding.significantOther.layoutParams.width = 160
                binding.significantOther.requestLayout()
            }
            3 -> {
                binding.family.layoutParams.height = 160
                binding.family.layoutParams.width = 160
                binding.family.requestLayout()

                binding.friend.layoutParams.height = 160
                binding.friend.layoutParams.width = 160
                binding.friend.requestLayout()

                binding.significantOther.layoutParams.height = 200
                binding.significantOther.layoutParams.width = 200
                binding.significantOther.requestLayout()
            }
            4 -> {
                binding.family.layoutParams.height = 160
                binding.family.layoutParams.width = 160
                binding.family.requestLayout()

                binding.friend.layoutParams.height = 160
                binding.friend.layoutParams.width = 160
                binding.friend.requestLayout()

                binding.significantOther.layoutParams.height = 160
                binding.significantOther.layoutParams.width = 160
                binding.significantOther.requestLayout()
            }
        }
    }

    private fun clearErrors() {
        with(binding) {
            contactName.error = null
            contactNumber.error = null
        }
    }

    private fun openDialog() {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Exito").setMessage("Se ha agregado tu contacto!")
            .setPositiveButton("OK") { _, _ -> }
        val dialogo = builder.create()
        dialogo.show()
    }

    private fun openDialog(body: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Sin conexion").setMessage(body)
            .setPositiveButton("OK") { _, _ -> }
        val dialogo = builder.create()
        dialogo.show()
    }


}