package com.example.tucompa

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TuCompa : Application() {
}