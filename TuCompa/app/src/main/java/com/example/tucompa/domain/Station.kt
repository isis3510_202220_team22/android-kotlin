package com.example.tucompa.domain

import com.example.tucompa.data.database.entitites.StationEntity
import com.example.tucompa.data.model.StationModel

data class Station(
    val latitude: Double,
    val longitude: Double,
    val name: String
)

fun StationModel.toDomain() = Station(latitude, longitude, name)
fun StationEntity.toDomain() = Station(latitude, longitude, name)