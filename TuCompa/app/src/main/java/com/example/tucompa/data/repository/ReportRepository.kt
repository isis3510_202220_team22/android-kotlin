package com.example.tucompa.data.repository

import com.example.tucompa.data.database.AppDatabase
import com.example.tucompa.data.database.entitites.toCurrentReportEntity
import com.example.tucompa.data.model.CurrentReportModel
import com.example.tucompa.data.model.ReportModel
import com.example.tucompa.data.network.ReportService
import com.example.tucompa.domain.CurrentReport
import com.example.tucompa.domain.Report
import com.example.tucompa.domain.toDomain
import com.google.firebase.Timestamp
import kotlinx.coroutines.*
import javax.inject.Inject

class ReportRepository @Inject constructor(private val reportService: ReportService, private val appDatabase: AppDatabase){

    private val currentReportDao = appDatabase.currentReportDao()

    suspend fun getCurrentStationReport():CurrentReport? {
        val report: CurrentReport? = withContext(Dispatchers.IO){
            val response = currentReportDao.getAll()

            if(response.isNotEmpty()){
                response[0].toDomain()
            } else {
                null
            }
        }
        return report
    }

    suspend fun postCurrentStationReport(dateTime: Timestamp,
                                         description: String,
                                         type: String,
                                         station: String):CurrentReport {
        val report: CurrentReport = withContext(Dispatchers.IO){
            val response = CurrentReportModel(dateTime, description, type, station)

            runBlocking {
                currentReportDao.deleteAll()
                currentReportDao.insertAll(response.toCurrentReportEntity())
            }
            response.toDomain()
        }
        return report
    }

    suspend fun clearCurrentStationReport(){
        GlobalScope.launch(Dispatchers.IO) {
            currentReportDao.deleteAll()
        }
    }

    suspend fun createStationReportInDatabase(
        dateTime: Timestamp,
        description: String,
        type: String,
        station: String
    ): Report {

        val report: ReportModel = withContext(Dispatchers.IO){
            val response: ReportModel = reportService.createStationReport(
                dateTime,
                description,
                type,
                station
            )

            response
        }

        return report.toDomain()
    }

    suspend fun getReportsFromLastHourByStation(stationName: String) : List<Report> {
        var reports : List<ReportModel>? = reportService.getReportsFromLastHourByStation(stationName.replace(" ", ""))

        if(reports.isNullOrEmpty()) {
            reports = emptyList()
        }

        return reports.map { report -> report.toDomain() }
    }
}