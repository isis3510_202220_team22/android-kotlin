package com.example.tucompa.data.database.dao

import androidx.room.*
import com.example.tucompa.data.database.entitites.CurrentUserEntity

@Dao
interface CurrentUserDao {
    @Query("SELECT * FROM current_user")
    suspend fun getAll(): List<CurrentUserEntity>

    @Query("SELECT * FROM current_user WHERE email = :userEmail")
    suspend fun getUserById(userEmail: String): CurrentUserEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg users: CurrentUserEntity)

    @Query("DELETE FROM current_user")
    suspend fun deleteAll()

    @Delete
    suspend fun delete(user: CurrentUserEntity)
}