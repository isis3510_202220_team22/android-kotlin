package com.example.tucompa.data.database.entitites

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.tucompa.data.model.StationModel
import java.util.*

@Entity(tableName = "station")
data class StationEntity (
    @PrimaryKey val name: String,
    @ColumnInfo(name = "latitude") val latitude: Double,
    @ColumnInfo(name = "longitude") val longitude: Double
)

fun StationModel.toEntity() = StationEntity(name, latitude, longitude)