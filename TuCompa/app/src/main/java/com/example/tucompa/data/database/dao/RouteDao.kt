package com.example.tucompa.data.database.dao

import androidx.room.*
import com.example.tucompa.data.database.entitites.RouteEntity

@Dao
interface RouteDao {
    @Query("SELECT * FROM route")
    suspend fun getAll(): List<RouteEntity>

    @Query("SELECT * FROM route WHERE id = :id")
    suspend fun getRouteById(id: String): RouteEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg routes: RouteEntity)

    @Query("DELETE FROM route WHERE id = :id")
    suspend fun deleteById(id: String)

    @Delete
    suspend fun delete(route: RouteEntity)
}