package com.example.tucompa.ui.view.listeners

import com.example.tucompa.domain.UserRating

interface RatingListener {
    fun onOptionSelected(userRating: UserRating, rate : Float)
}