package com.example.tucompa.data.repository

import android.util.ArrayMap
import android.util.Log
import com.example.tucompa.data.database.AppDatabase
import com.example.tucompa.data.database.entitites.RouteEntity
import com.example.tucompa.data.database.entitites.UserRatingEntity
import com.example.tucompa.data.database.entitites.toEntity
import com.example.tucompa.data.model.RouteModel
import com.example.tucompa.data.model.StationModel
import com.example.tucompa.data.model.UserRatingModel
import com.example.tucompa.data.network.RouteService
import com.example.tucompa.data.network.StationService
import com.example.tucompa.domain.Route
import com.example.tucompa.domain.UserRating
import com.example.tucompa.domain.toDomain
import com.google.firebase.Timestamp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.math.pow
import kotlin.math.sqrt

class RouteRepository @Inject constructor(private val routeService: RouteService, private val stationService: StationService, private val appDatabase: AppDatabase) {

    private val routeDao = appDatabase.routeDao()
    private val userRatingDao = appDatabase.userRatingDao()

    suspend fun createRoute(departureDate: Timestamp,
                            destinationStationName: String,
                            originStationName : String,
                            userEmail: String): Route? {

        val route : RouteModel? = routeService.createRoute(
            departureDate,
            destinationStationName,
            originStationName,
            userEmail
        )

        if(route != null) {
            routeDao.insertAll(route.toEntity())
        }

        return route?.toDomain()
    }

    suspend fun getRoute(routeId: String, isConnectivity: Boolean) : Route? {
        return withContext(Dispatchers.IO) {
            if (isConnectivity) {
                val route : RouteModel? = routeService.getRoute(routeId)
                Log.i("RouteRepository", "Fetched route $route, from server")

                if(route != null) {
                    routeDao.insertAll(route.toEntity())
                    route.toDomain()
                } else {
                    val routeFromDb : RouteEntity = routeDao.getRouteById(routeId)
                    Log.i("RouteRepository", "Fetched route $routeFromDb, from local database")
                    if(routeFromDb != null) {
                        routeFromDb.toDomain()
                    } else {
                        null
                    }
                }
            } else {
                val routeFromDb : RouteEntity = routeDao.getRouteById(routeId)
                Log.i("RouteRepository", "Fetched route $routeFromDb, from local database")
                if(routeFromDb != null) {
                    routeFromDb.toDomain()
                } else {
                    null
                }
            }
        }
    }

    suspend fun getMostCommonRoute(userEmail: String, latitude : Double, longitude : Double) : List<String> {
        return withContext(Dispatchers.Default) {
            var stationList : MutableList<String> = mutableListOf()
            val stations = stationService.getStations()
            Log.i("RouteRepository", "Got all stations $stations")
            val routes = routeService.getAllRoutesFromUser(userEmail)
            Log.i("RouteRepository", "Got all routes $routes")

            if(!stations.isNullOrEmpty()) {
                var closerStation = stations[0]
                var stationLatitude : Double = closerStation.latitude
                var stationLongitude : Double = closerStation.longitude
                var lowestDistance = sqrt((stationLatitude - latitude).pow(2) + (stationLongitude - longitude).pow(2))
                Log.i("RouteRepository", "Lowest distance $lowestDistance")

                var station : StationModel

                for(i in stations.indices) {
                    station = stations[i]

                    Log.i("RouteRepository", "Reading station $station")
                    stationLatitude = station.latitude
                    stationLongitude = station.longitude

                    var distance = sqrt((stationLatitude - latitude).pow(2) + (stationLongitude - longitude).pow(2))
                    Log.i("RouteRepository", "Station $station calculated distance $distance")
                    if(distance < lowestDistance) {
                        closerStation = station
                        lowestDistance = distance
                        Log.i("RouteRepository", "New lowest distance $lowestDistance")
                    }
                }

                Log.i("RouteRepository", "Calculated closer station to ($latitude,$longitude) $closerStation")

                stationList.add(closerStation.name)
                val countStations : ArrayMap<String,Int> = ArrayMap()
                var route : RouteModel

                for(i in routes.indices) {
                    route = routes[i]

                    if(route.originStationName == closerStation.name) {
                        val destinationStationName = route.destinationStationName
                        Log.i("RouteRepository", "Reading station $destinationStationName")
                        countStations[destinationStationName] = (countStations[destinationStationName] ?: 0) + 1
                        Log.i("RouteRepository", "Updated count to ${countStations[destinationStationName]}")

                    }
                }
                
                stationList.add(countStations.maxByOrNull { it.value }?.key ?: "")
            }
            stationList
        }
    }

    suspend fun getAllRoutesUserIsBeen(userEmail: String, isConnection : Boolean) : List<Route> {
        return withContext(Dispatchers.Default) {
            val routesUserIsIn : MutableList<Route> = mutableListOf()

            if(isConnection) {
                val routes = routeService.getRoutes()
                Log.i("RouteRepository", "Got routes $routes from server")
                if(routes.isNotEmpty()) {
                    var route: RouteModel
                    for(i in routes.indices) {
                        route = routes[i]
                        var userIsInRoute = false
                        val users = routeService.getUserRatingsFromRoute(route.id)

                        if(route.isFinished) {
                            for(j in users.indices) {
                                if(users[j].userEmail == userEmail) {
                                    userIsInRoute = true
                                }
                            }
                        }

                        if(userIsInRoute) {
                            routesUserIsIn.add(route.toDomain())
                            routeDao.insertAll(route.toEntity()) //Persist in database
                        }
                    }
                }

                routesUserIsIn
            } else {
                val routes = routeDao.getAll()
                Log.i("RouteRepository", "Got routes $routes from room")

                if(routes.isNotEmpty()) {
                    var route: RouteEntity
                    for(i in routes.indices) {
                        route = routes[i]

                        if(route.isFinished) {
                            routesUserIsIn.add(route.toDomain())
                        }
                    }
                }
                routesUserIsIn.sortedByDescending { route -> route.departureDate }
            }
        }
    }

    suspend fun deleteRouteFromDatabase(routeId: String) {
        routeDao.deleteById(routeId)
        routeService.deleteRoute(routeId)
    }

    suspend fun updateRouteIsFinishedInDatabase(routeId: String, newIsFinished: Boolean) : Route? {
        return  routeService.updateRouteIsFinished(routeId,newIsFinished)?.toDomain()
    }

    suspend fun getUserRatingsFromRouteFromDatabase(routeId: String, isConnectivity : Boolean) : List<UserRating> {
        return withContext(Dispatchers.IO) {
            if(isConnectivity) {
                val response: List<UserRatingModel> = routeService.getUserRatingsFromRoute(routeId)

                if(response.isNotEmpty()) {
                    response.forEach { userRating -> userRatingDao.insertAll(userRating.toEntity()) }
                    response.map { userRating -> userRating.toDomain() }
                }

                else {
                    val dbResponse : List<UserRatingEntity> = userRatingDao.getAll()
                    dbResponse.map { userRating -> userRating.toDomain() }
                }
            } else {
                val dbResponse : List<UserRatingEntity> = userRatingDao.getAll()
                dbResponse.map { userRating -> userRating.toDomain() }
            }
        }
    }

    suspend fun createUserRatingInDatabase(routeId: String, pending : Boolean, userEmail: String): UserRating? {
        val userRating : UserRatingModel? = routeService.postUserRating(routeId, pending, userEmail)
        if(userRating != null) {
            userRatingDao.insertAll(userRating.toEntity())
        }
        return  userRating?.toDomain()
    }

    suspend fun updateUserRatingPendingInDatabase(routeId: String, userEmail: String, newPendingStatus : Boolean) : UserRating? {
        val userRating : UserRatingModel? = routeService.updateUserRatingPending(routeId,userEmail,newPendingStatus)
        if(userRating != null) {
            userRatingDao.insertAll(userRating.toEntity())
        }
        return userRating?.toDomain()
    }

    suspend fun updateUserRatingRateInDatabase(routeId: String, userEmail: String, newRate : Float) : UserRating? {
        val userRating : UserRatingModel? = routeService.updateUserRatingRate(routeId,userEmail,newRate)
        if(userRating != null) {
            userRatingDao.insertAll(userRating.toEntity())
        }
        return userRating?.toDomain()
    }

    suspend fun deleteUserRatingFromDatabase(routeId: String, userEmail: String) {
        userRatingDao.deleteById(userEmail)
        routeService.deleteUserRating(routeId,userEmail)
    }

    fun getRoutesConditional(date: Timestamp?, destinationStationName: String?, originStationName: String?):MutableList<Route>{
        val routes = mutableListOf<Route>()
        val routesReceived : MutableList<RouteModel>? = routeService.getRoutesConditional(date, destinationStationName, originStationName)

        routesReceived?.forEach{
            var element = it.toDomain()
            routes.add(element)
        }
        return routes
    }
}