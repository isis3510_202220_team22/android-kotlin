package com.example.tucompa.data.model

import com.google.firebase.Timestamp

class CurrentReportModel(
    var dateTime: Timestamp = Timestamp.now(),
    var description: String = "",
    var type: String = "",
    var station: String = ""
)