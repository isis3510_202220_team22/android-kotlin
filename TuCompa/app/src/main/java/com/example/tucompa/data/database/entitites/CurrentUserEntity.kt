package com.example.tucompa.data.database.entitites

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.tucompa.data.model.UserModel
import java.util.*

@Entity(tableName = "current_user")
data class CurrentUserEntity(
    @ColumnInfo(name = "current_route") val currentRoute: String,
    @PrimaryKey val email: String,
    @ColumnInfo(name = "image") val image: String,
    @ColumnInfo(name = "is_in_route") val isInRoute: Boolean,
    @ColumnInfo(name = "last_route") val lastRoute: Date,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "quantity_rating") val quantityRating: Int,
    @ColumnInfo(name = "streak") val streak: Int,
    @ColumnInfo(name = "total_rating") val totalRating: Double,
)

fun UserModel.toCurrentUserEntity() = CurrentUserEntity(currentRoute, email, image, isInRoute, lastRoute.toDate(), name, quantityRating, streak, totalRating)