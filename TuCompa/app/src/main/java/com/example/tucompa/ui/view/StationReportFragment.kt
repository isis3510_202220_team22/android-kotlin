package com.example.tucompa.ui.view

import android.app.AlertDialog
import android.content.ContentValues
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.tu.StationViewModel
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.core.ActivityExtentions.createDialog
import com.example.tucompa.databinding.FragmentStationReportBinding
import com.example.tucompa.ui.viewmodel.ReportViewModel
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.google.firebase.Timestamp
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class StationReportFragment : Fragment(R.layout.fragment_station_report) {

    private lateinit var binding: FragmentStationReportBinding
    private var activity: AppActivity? = null

    private val stationViewModel: StationViewModel by viewModels()
    private val reportViewModel: ReportViewModel by viewModels()

    private lateinit var connectivityLiveData: ConnectivityLiveData

    private var day: Int = 0
    private var dateTime: Timestamp = Timestamp.now()
    private var description: String = ""
    private var type: String = ""
    private var station: String = ""
    private var lastTimeClicked: Long = 0

    private var mLastClickTime = 0L

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStationReportBinding.inflate(inflater, container, false)

        activity = getActivity() as AppActivity

        connectivityLiveData = context?.let { ConnectivityLiveData(it) }!!

        val bar: View? = activity?.findViewById(R.id.lyBack) ?: null
        if (bar != null) {
            bar.visibility = View.VISIBLE
        }

        val tvTop: TextView? = activity?.findViewById(R.id.txtNavTittle) ?: null
        if (tvTop != null) {
            tvTop.text = "Reportar situacion anomala"
        }

        lifecycleScope.launch {
            reportViewModel.allEventsFlow.collect { event ->
                when (event) {
                    is ReportViewModel.AllEvents.Error -> {
                        when (event.error) {
                            Constants.NON_EXISTENT_STATION_ERROR -> {
                                clearErrors()
                                binding.reportlocation.error =
                                    Constants.CREATE_REPORT_ERRORS_MESSAGES[
                                            Constants.NON_EXISTENT_STATION_ERROR
                                    ]
                                binding.reportlocation.requestFocus()
                            }
                            Constants.EMPTY_TYPE -> {
                                clearErrors()
                                binding.reportsituation.error =
                                    Constants.CREATE_REPORT_ERRORS_MESSAGES[
                                            Constants.EMPTY_TYPE
                                    ]
                                binding.reportsituation.requestFocus()
                            }
                            Constants.EMPTY_DESCRIPTION -> {
                                clearErrors()
                                binding.reportinformation.error =
                                    Constants.CREATE_REPORT_ERRORS_MESSAGES[
                                            Constants.EMPTY_DESCRIPTION
                                    ]
                                binding.reportinformation.requestFocus()
                            }
                            Constants.SHORT_DESCRIPTION -> {
                                clearErrors()
                                binding.reportinformation.error =
                                    Constants.CREATE_REPORT_ERRORS_MESSAGES[
                                            Constants.SHORT_DESCRIPTION
                                    ]
                                binding.reportinformation.requestFocus()
                            }
                            Constants.FUTURE_DEPARTURE_TIME_ERROR -> {
                                clearErrors()
                                createDialog(
                                    "Error",
                                    Constants.CREATE_REPORT_ERRORS_MESSAGES[Constants.FUTURE_DEPARTURE_TIME_ERROR].toString()
                                )
                                binding.reportform.requestFocus()
                            }
                        }
                    }
                    is ReportViewModel.AllEvents.Message -> {
                        clearErrors()
                        openDialog()
                    }
                    else -> {
                        Log.d(ContentValues.TAG, "listenToChannels: No event received so far")
                    }
                }
            }
        }

        addObservers()
        setup()

        return binding.root
    }

    private fun setup() {

        reportViewModel.getCurrentReport()

        binding.apply {

            val situationOptions = resources.getStringArray(R.array.stationreportsituations)
            val arrayAdapter =
                context?.let {
                    ArrayAdapter(
                        it,
                        android.R.layout.simple_list_item_1,
                        situationOptions
                    )
                }

            val autocompleteTV = situationoptions

            autocompleteTV.setAdapter(arrayAdapter)

            reportViewModel.currentReportModel.observe(viewLifecycleOwner) {
                if (it != null) {

                    reportinformationtext.setText(it.description)
                    situationoptions.setText(it.type, false)
                    reportlocationtext.setText(it.station, false)

                    val date = it.dateTime.toDate()
                    dateTime = it.dateTime
                    val fecha1 = Calendar.getInstance()
                    fecha1.time = dateTime.toDate()
                    day = fecha1.get(Calendar.DAY_OF_YEAR)

                    val datePicker =
                        MaterialDatePicker.Builder.datePicker()
                            .setTitleText("Selecciona un dia")
                            .setSelection(date.time)
                            .build()

                    btnDate.setOnClickListener {
                        if (!isClickRecently()) {
                            datePicker.show(requireActivity().supportFragmentManager, "MATERIAL_DATE_PICKER")
                        }
                    }

                    datePicker.addOnPositiveButtonClickListener {
                        val fecha = Calendar.getInstance()
                        fecha.setTimeInMillis(it)
                        day = fecha.get(Calendar.DAY_OF_YEAR) + 1
                        datetext.setText(datePicker.headerText)

                        val calendar = Calendar.getInstance()
                        calendar.set(Calendar.DAY_OF_YEAR, day)
                        calendar.set(Calendar.HOUR_OF_DAY, dateTime.toDate().hours)
                        calendar.set(Calendar.MINUTE, dateTime.toDate().minutes)
                        calendar.set(Calendar.SECOND, 0)

                        dateTime = Timestamp(Date(calendar.timeInMillis))

                        saveCurrentReport(dateTime)
                    }

                    val timePicker =
                        MaterialTimePicker.Builder()
                            .setTimeFormat(TimeFormat.CLOCK_12H)
                            .setHour(date.hours)
                            .setMinute(date.minutes)
                            .setTitleText("Selecciona una hora")
                            .build()

                    val timeNow = Calendar.getInstance()
                    timeNow.time = date

                    val monthname = SimpleDateFormat("MMM").format(timeNow.time)

                    datetext.setText(
                        "${monthname} ${checkDigit(timeNow.get(Calendar.DAY_OF_MONTH))}, ${
                            timeNow.get(
                                Calendar.YEAR
                            )
                        }"
                    )
                    hourtext.setText(
                        "${checkDigit(timeNow.get(Calendar.HOUR_OF_DAY))}:${
                            checkDigit(
                                timeNow.get(
                                    Calendar.MINUTE
                                )
                            )
                        }"
                    )

                    btnHour.setOnClickListener {
                        if (!isClickRecently()) {
                            timePicker.show(
                                requireActivity().supportFragmentManager,
                                "MATERIAL_DATE_PICKER"
                            )
                        }
                    }

                    timePicker.addOnPositiveButtonClickListener {
                        hourtext.setText("${timePicker.hour}:${timePicker.minute}")

                        val calendar = Calendar.getInstance()
                        if(day != 0) calendar.set(Calendar.DAY_OF_YEAR, day)
                        calendar.set(Calendar.HOUR_OF_DAY, timePicker.hour)
                        calendar.set(Calendar.MINUTE, timePicker.minute)
                        calendar.set(Calendar.SECOND, 0)

                        dateTime = Timestamp(Date(calendar.timeInMillis))

                        saveCurrentReport(dateTime)
                    }
                } else {

                    val datePicker =
                        MaterialDatePicker.Builder.datePicker()
                            .setTitleText("Selecciona un dia")
                            .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
                            .build()

                    btnDate.setOnClickListener {
                        if (!isClickRecently()) {
                            datePicker.show(
                                requireActivity().supportFragmentManager,
                                "MATERIAL_DATE_PICKER"
                            )
                        }
                    }

                    datePicker.addOnPositiveButtonClickListener {
                        val fecha = Calendar.getInstance()
                        fecha.setTimeInMillis(it)
                        day = fecha.get(Calendar.DAY_OF_YEAR) + 1
                        datetext.setText(datePicker.headerText)

                        val calendar = Calendar.getInstance()
                        calendar.set(Calendar.DAY_OF_YEAR, day)
                        calendar.set(Calendar.HOUR_OF_DAY, dateTime.toDate().hours)
                        calendar.set(Calendar.MINUTE, dateTime.toDate().minutes)
                        calendar.set(Calendar.SECOND, 0)

                        dateTime = Timestamp(Date(calendar.timeInMillis))

                        saveCurrentReport(dateTime)
                    }

                    val timeNow = Calendar.getInstance()

                    val timePicker =
                        MaterialTimePicker.Builder()
                            .setTimeFormat(TimeFormat.CLOCK_12H)
                            .setHour(timeNow.get(Calendar.HOUR_OF_DAY))
                            .setMinute(timeNow.get(Calendar.MINUTE))
                            .setTitleText("Selecciona una hora")
                            .build()

                    val monthname = SimpleDateFormat("MMM").format(timeNow.time)

                    datetext.setText(
                        "${monthname} ${checkDigit(timeNow.get(Calendar.DAY_OF_MONTH))}, ${
                            timeNow.get(
                                Calendar.YEAR
                            )
                        }"
                    )
                    hourtext.setText(
                        "${checkDigit(timeNow.get(Calendar.HOUR_OF_DAY))}:${
                            checkDigit(
                                timeNow.get(
                                    Calendar.MINUTE
                                )
                            )
                        }"
                    )

                    btnHour.setOnClickListener {
                        if (!isClickRecently()) {
                            timePicker.show(
                                requireActivity().supportFragmentManager,
                                "MATERIAL_DATE_PICKER"
                            )
                        }
                    }

                    timePicker.addOnPositiveButtonClickListener {
                        hourtext.setText("${timePicker.hour}:${timePicker.minute}")

                        val calendar = Calendar.getInstance()
                        if(day != 0) calendar.set(Calendar.DAY_OF_YEAR, day)
                        calendar.set(Calendar.HOUR_OF_DAY, timePicker.hour)
                        calendar.set(Calendar.MINUTE, timePicker.minute)
                        calendar.set(Calendar.SECOND, 0)

                        dateTime = Timestamp(Date(calendar.timeInMillis))

                        saveCurrentReport(dateTime)
                    }
                }
            }

            situationoptions.setOnFocusChangeListener{ v, hasFocus ->
                if(!hasFocus) {
                    saveCurrentReport(dateTime)
                }
            }

            reportlocationtext.setOnFocusChangeListener{ v, hasFocus ->
                if(!hasFocus) {
                    saveCurrentReport(dateTime)
                }
            }

            reportinformationtext.setOnFocusChangeListener{ v, hasFocus ->
                if(!hasFocus) {
                    saveCurrentReport(dateTime)
                }
            }

            connectivityLiveData.checkValidNetworks()

            connectivityLiveData.observe(viewLifecycleOwner) { isNetwork ->

                stationViewModel.getStationNames(isNetwork)

                btnsubmitreport.setOnClickListener {
                    if (isNetwork) {
                        val calendar = Calendar.getInstance()
                        calendar.set(Calendar.DAY_OF_YEAR, day)
                        calendar.set(Calendar.HOUR_OF_DAY, dateTime.toDate().hours)
                        calendar.set(Calendar.MINUTE, dateTime.toDate().minutes)
                        calendar.set(Calendar.SECOND, 0)

                        dateTime = Timestamp(Date(calendar.timeInMillis))

                        description = reportinformationtext.text.toString().trim()
                        type = situationoptions.text.toString().trim()
                        station = reportlocationtext.text.toString().trim()

                        reportViewModel.validateAndCreateStationReport(
                            dateTime,
                            description,
                            type,
                            station,
                            isNetwork
                        )

                        reportinformationtext.setText("")
                        reportinformationtext.clearFocus()
                        situationoptions.setText("")
                        situationoptions.clearFocus()
                        reportlocationtext.setText("")
                        reportlocationtext.clearFocus()
                        description = ""
                        type = ""
                        station = ""
                        day = 0
                        dateTime = Timestamp.now()

                    } else {
                        openDialog(Constants.REPORT_CONNECTION_ERROR)
                    }
                }
            }
        }
    }

    private fun addObservers() {

        stationViewModel.stationNames.observe(viewLifecycleOwner) {
            val adapter = this.activity?.let { it1 ->
                ArrayAdapter(
                    it1, android.R.layout.simple_list_item_1,
                    it
                )
            }
            binding.reportlocationtext.setAdapter(adapter)
        }


        reportViewModel.loading.observe(viewLifecycleOwner) {
            updateLoadingBar(it)
        }
    }

    private fun clearErrors() {
        with(binding) {
            reportlocation.error = null
            reportinformation.error = null
            reportsituation.error = null
        }
    }

    private fun openDialog() {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Exito").setMessage("Tu reporte ha sido publicado!")
            .setPositiveButton("OK") { _, _ -> }
        val dialogo = builder.create()
        dialogo.show()
    }

    private fun openDialog(body: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Sin conexion").setMessage(body)
            .setPositiveButton("OK") { _, _ -> }
        val dialogo = builder.create()
        dialogo.show()
    }

    private fun checkDigit(number: Int): String {
        var response = ""
        if (number <= 9) {
            response = "0${number}"
        } else {
            response = "${number}"
        }
        return response
    }

    private fun updateLoadingBar(isLoading: Boolean) {
        with(binding) {
            if (isLoading) {
                pbLoading.visibility = View.VISIBLE
                btnsubmitreport.visibility = View.GONE
            } else {
                pbLoading.visibility = View.GONE
                btnsubmitreport.visibility = View.VISIBLE
            }
        }
    }

    private fun saveCurrentReport(
        dateTime: Timestamp
    ) {
        var description = binding.reportinformationtext.text.toString().trim()
        var type = binding.situationoptions.text.toString().trim()
        var station = binding.reportlocationtext.text.toString().trim()
        reportViewModel.postCurrentReport(dateTime, description, type, station)
    }

    fun isClickRecently(): Boolean {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return true
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        return false
    }
}