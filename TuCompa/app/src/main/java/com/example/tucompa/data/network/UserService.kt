package com.example.tucompa.data.network;

import android.content.ContentValues.TAG
import android.util.Log
import com.example.tucompa.Constants
import com.example.tucompa.data.model.UserModel
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await

import javax.inject.Inject;

class UserService @Inject constructor(private val db: FirebaseFirestore) {
    suspend fun getUser(email: String): UserModel? {
        val collectionUsers = db.collection(Constants.USER_COLLECTION_NAME).document(email)
        var currentUser: UserModel? = null

        val job = GlobalScope.launch(Dispatchers.IO) {
            currentUser = collectionUsers.get().await().toObject(UserModel::class.java)
            Log.d("UserService", "DocumentSnapshot data : ${currentUser}")
        }

        runBlocking {
            job.join()
        }
        return currentUser
    }

    suspend fun updateUserLastRouteDate(email : String, newLastRoute: Timestamp): UserModel? {
        val collectionUsers = db.collection(Constants.USER_COLLECTION_NAME).document(email)

        val job = GlobalScope.launch(Dispatchers.IO) {
            collectionUsers.update("lastRoute", newLastRoute)
                .addOnSuccessListener{ Log.d(TAG, "DocumentSnapshot successfully updated!") }
        }

        runBlocking {
            job.join()
        }

        return getUser(email)
    }

    suspend fun updateUserStreak(email : String, newStreak: Int): UserModel? {
        val collectionUsers = db.collection(Constants.USER_COLLECTION_NAME).document(email)

        val job = GlobalScope.launch(Dispatchers.IO) {
            collectionUsers.update("streak", newStreak)
                .addOnSuccessListener{ Log.d(TAG, "DocumentSnapshot successfully updated!") }
        }

        runBlocking {
            job.join()
        }

        return getUser(email)
    }

    suspend fun updateUserIsInRoute(email: String, newIsInRoute : Boolean) : UserModel? {
        val collectionUsers = db.collection(Constants.USER_COLLECTION_NAME).document(email)

        collectionUsers.update("isInRoute", newIsInRoute).addOnSuccessListener {
            Log.d(TAG, "UserService: Successfully updated user $email route status to $newIsInRoute")
        }.await()

        return getUser(email)
    }

    suspend fun updateUserActualRoute(email: String, newRouteId : String) : UserModel? {
        val collectionUsers = db.collection(Constants.USER_COLLECTION_NAME).document(email)

        collectionUsers.update("currentRoute", newRouteId).addOnSuccessListener {
            Log.d(TAG, "UserService: Successfully updated user $email route current route to $newRouteId")
        }.await()

        return getUser(email)
    }

    suspend fun updateUserRating(email: String, newRating : Float) : UserModel? {
        val collectionUsers = db.collection(Constants.USER_COLLECTION_NAME).document(email)

        collectionUsers.update("totalRating", newRating).addOnSuccessListener {
            Log.d(TAG, "UserService: Successfully updated user $email rating to $newRating")
        }.await()

        return getUser(email)
    }

    suspend fun updateUserQuantityRating(email: String, newQRating : Int) : UserModel? {
        val collectionUsers = db.collection(Constants.USER_COLLECTION_NAME).document(email)

        collectionUsers.update("quantityRating", newQRating).addOnSuccessListener {
            Log.d(TAG, "UserService: Successfully updated user $email quantity rating to $newQRating")
        }.await()

        return getUser(email)
    }
}
