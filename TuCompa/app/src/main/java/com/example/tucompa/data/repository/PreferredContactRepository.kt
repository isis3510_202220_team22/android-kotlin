package com.example.tucompa.data.repository

import android.util.Log
import com.example.tucompa.data.database.AppDatabase
import com.example.tucompa.data.database.entitites.toEntity
import com.example.tucompa.data.model.PreferredContactModel
import com.example.tucompa.domain.PreferredContact
import com.example.tucompa.domain.toDomain
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PreferredContactRepository @Inject constructor(private val appDatabase: AppDatabase) {
    private val preferredContactDao = appDatabase.preferredContactDao()

    suspend fun postPreferredContact(name: String, number: String, image: String): PreferredContact{
        val preferredContact: PreferredContact = withContext(Dispatchers.IO){
            val response = PreferredContactModel(name, number, image)

            preferredContactDao.insertAll(response.toEntity())

            response.toDomain()
        }

        Log.i("PrefContactRepository", "Posted contact $preferredContact to disk")

        return preferredContact
    }

    suspend fun getPreferredContacts(): List<PreferredContact> {
        return withContext(Dispatchers.IO){
            val dbResponse = preferredContactDao.getAll()

            if (!dbResponse.isNullOrEmpty()){
                Log.i("PrefContactRepository", "Retrieved $dbResponse from disk")
                dbResponse.map { preferredContact -> preferredContact.toDomain()}
            } else {
                emptyList()
            }
        }
    }

    suspend fun getPreferredContact(number: String): PreferredContact{
        val preferredContact : PreferredContact = withContext(Dispatchers.IO){
            val dbResponse = preferredContactDao.getContactByPhoneNumber(number)

            if(dbResponse != null){
                dbResponse.toDomain()
            } else {
                PreferredContact()
            }
        }

        return preferredContact
    }

    suspend fun deletePreferredContacts() {
        GlobalScope.launch(Dispatchers.IO) {
            preferredContactDao.deleteAll()
        }
    }
}