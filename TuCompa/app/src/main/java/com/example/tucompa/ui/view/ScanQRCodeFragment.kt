package com.example.tucompa.ui.view

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.example.tucompa.Constants
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.databinding.FragmentScanQRCodeBinding
import com.example.tucompa.ui.viewmodel.QRScanViewModel
import com.example.tucompa.ui.viewmodel.UserProfileViewModel
import com.example.tucompa.ui.viewmodel.UserRegistrationViewModel
import com.google.zxing.integration.android.IntentIntegrator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ScanQRCodeFragment : AppCompatActivity() {

    private lateinit var binding: FragmentScanQRCodeBinding
    private val QRScanViewModel: QRScanViewModel by viewModels()

    private val userProfileViewModel : UserProfileViewModel by viewModels()

    private lateinit var connectivityLiveData: ConnectivityLiveData

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FragmentScanQRCodeBinding.inflate(layoutInflater)

        connectivityLiveData = ConnectivityLiveData(this)

        setContentView(binding.root)

        QRScanViewModel.wagonModel.observe(this, Observer {
            if (it != null) {
                binding.scandescription.text = it.description
            }
        })

        connectivityLiveData.checkValidNetworks()

        connectivityLiveData.observe(this) { isNetwork ->
            binding.scanbutton.setOnClickListener {
                if (isNetwork) {
                    initScanner()
                } else {
                    openDialog("Sin conexion", Constants.SCAN_CONNECTION_ERROR)
                }
            }
        }

        binding.btnBack.setOnClickListener {
            onBackPressed()
        }

        lifecycleScope.launch {
            QRScanViewModel.allsearchWagonEventsFlow.collect { event ->
                when (event) {
                    is QRScanViewModel.AllEvents.Error -> {
                        when (event.error) {
                            Constants.WAGON_DOES_NOT_EXIST -> {
                                openDialog("No existe", Constants.WAGON_DOES_NOT_EXIST)
                                binding.scandescription.text = null
                            }
                        }
                    }
                }
            }
        }

        QRScanViewModel.logQRScanViewOpened(userProfileViewModel.currentUserModel.value?.email?:"")
    }

    private fun initScanner() {
        val integrator = IntentIntegrator(this)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
        integrator.setPrompt("Escanea un codigo QR")
        integrator.initiateScan()
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        connectivityLiveData.checkValidNetworks()

        if (connectivityLiveData.value == true) {
            val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                if (result.contents == null) {

                } else {
                    QRScanViewModel.currentWagon(result.contents, userProfileViewModel.currentUserModel.value?.email?:"")
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data)
            }
        } else {
            openDialog("Sin conexion", Constants.SCAN_CONNECTION_ERROR)
        }
    }

    private fun openDialog(title: String, body: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title).setMessage(body)
            .setPositiveButton("OK") { _, _ -> }
        val dialogo = builder.create()
        dialogo.show()
    }
}