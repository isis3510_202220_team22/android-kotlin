package com.example.tucompa.di

import android.app.Application
import androidx.room.Room
import com.example.tucompa.data.database.AppDatabase
import com.example.tucompa.data.repository.BaseAuthenticator
import com.example.tucompa.data.repository.FirebaseAuthenticator
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Singleton
    @Provides
    fun provideFireStoreDB(): FirebaseFirestore{
        var db = FirebaseFirestore.getInstance()
        return db
    }

    @Singleton
    @Provides
    fun provideAuthenticator() : BaseAuthenticator {
        return  FirebaseAuthenticator()
    }

    @Singleton
    @Provides
    fun provideFirebaseAnalytics() : FirebaseAnalytics {
        return Firebase.analytics
    }

    @Singleton
    @Provides
    fun provideAppDatabaseRoom(app : Application) =
        Room.databaseBuilder(
            app,
            AppDatabase::class.java, "app_database"
        ).build()
}