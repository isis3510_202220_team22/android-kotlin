package com.example.tucompa.ui.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tucompa.R
import com.example.tucompa.databinding.ItemRouteHistoryBinding
import com.google.firebase.firestore.auth.User
import com.squareup.picasso.Picasso
import okhttp3.Route
import java.util.Calendar

class RouteHistoryAdapter(private var routeList : List< com.example.tucompa.domain.Route >, private var adminUserList : MutableList<com.example.tucompa.domain.User>, private var actualUser : com.example.tucompa.domain.User) : RecyclerView.Adapter<RouteHistoryAdapter.ViewHolder>() {
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemRouteHistoryBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RouteHistoryAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_route_history, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.i("RouteHistoryAdapter", "Printing position position $position")
        val route : com.example.tucompa.domain.Route = routeList[position]
        val admin : com.example.tucompa.domain.User = adminUserList[position]

        with(holder.binding) {
            val calendar : Calendar = Calendar.getInstance()
            calendar.time = route.departureDate.toDate()

            var minuteText : String
            if(calendar.get(Calendar.MINUTE) < 10) {
                minuteText = "0"+calendar.get(Calendar.MINUTE).toString()
            } else {
                minuteText = calendar.get(Calendar.MINUTE).toString()
            }

            tvDepartureTime.text = "${calendar.get(Calendar.YEAR)}/${calendar.get(Calendar.MONTH)}/${calendar.get(Calendar.DAY_OF_MONTH)} - ${calendar.get(Calendar.HOUR_OF_DAY)}:${minuteText}"
            tvDepartureStation.text = route.originStationName
            tvDestinationStation.text = route.destinationStationName

            var adminName : String = admin.name

            if(actualUser.email == admin.email) {
                adminName += " (Tú)"
            }

            tvAdminName.text = adminName
            Picasso.get().load(admin.image).into(holder.binding.adminProfilePhoto)
        }
    }

    fun setActualUser(newActualUser: com.example.tucompa.domain.User) {
        actualUser = newActualUser
        notifyDataSetChanged()
    }

    fun addRoutes(newRoutes:  List<com.example.tucompa.domain.Route>) {
        routeList = newRoutes
        notifyDataSetChanged()
    }

    fun addAdminUser(newAdminUsers : MutableList<com.example.tucompa.domain.User>) {
        adminUserList = newAdminUsers
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = adminUserList.size
}