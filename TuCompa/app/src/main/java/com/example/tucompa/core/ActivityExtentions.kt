package com.example.tucompa.core

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.widget.Toast
import androidx.fragment.app.Fragment

object ActivityExtentions {
    fun Activity.toast(text: String, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, text, duration).show()
    }

    fun Fragment.createDialog(title : String, body : String) {
        val alertDialog = AlertDialog.Builder(activity)
        alertDialog.apply {
            setTitle(title)
            setMessage(body).setPositiveButton("Ok",
                DialogInterface.OnClickListener { dialog, id ->
                    dialog.dismiss()
                })
        }.create().show()
    }

    fun Activity.createDialog(title : String, body : String) {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.apply {
            setTitle(title)
            setMessage(body).setPositiveButton("Ok",
                DialogInterface.OnClickListener { dialog, id ->
                    dialog.dismiss()
                })
        }.create().show()
    }
}