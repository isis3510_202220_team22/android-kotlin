package com.example.tucompa.data.model

data class StationModel(
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val name: String = ""
)