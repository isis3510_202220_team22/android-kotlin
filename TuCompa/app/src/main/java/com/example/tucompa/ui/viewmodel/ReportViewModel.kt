package com.example.tucompa.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tucompa.Constants
import com.example.tucompa.data.repository.ReportRepository
import com.example.tucompa.data.repository.StationRepository
import com.example.tucompa.domain.CurrentReport
import com.example.tucompa.domain.Report
import com.google.firebase.Timestamp
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ReportViewModel @Inject constructor(
    private val reportRepository: ReportRepository,
    private val stationRepository: StationRepository
) : ViewModel() {

    private val lastCreatedStationReport = MutableLiveData<Report>()
    val lastStationReport get() = lastCreatedStationReport

    private val reportsQuantity = MutableLiveData<Map<String,Int>>()
    val qReports get() = reportsQuantity

    private val eventsChannel = Channel<AllEvents>()

    private val isLoading = MutableLiveData<Boolean>(false)
    val loading get() = isLoading

    val currentReportModel = MutableLiveData<CurrentReport?>()

    val allEventsFlow = eventsChannel.receiveAsFlow()

    fun getCurrentReport() =
        viewModelScope.launch {
            val currentReport = reportRepository.getCurrentStationReport()
            currentReportModel.postValue(currentReport)
        }

    fun postCurrentReport(dateTime: Timestamp,
                          description: String,
                          type: String,
                          station: String) =
        viewModelScope.launch {
            val currentReport = reportRepository.postCurrentStationReport(
                dateTime,
                description,
                type,
                station
            )
            currentReportModel.postValue(currentReport)
        }

    fun validateAndCreateStationReport(
        dateTime: Timestamp,
        description: String,
        type: String,
        station: String,
        isConnection : Boolean
    ) = viewModelScope.launch {
        isLoading.postValue(true)
        val stations = stationRepository.getStations(isConnection).map { station -> station.name }
        when {
            dateTime.compareTo(Timestamp.now()) > 0 ->
                eventsChannel.send(
                    AllEvents.Error(Constants.FUTURE_DEPARTURE_TIME_ERROR)
                )
            station !in stations ->
                eventsChannel.send(
                    AllEvents.Error(Constants.NON_EXISTENT_STATION_ERROR)
                )
            type == "" ->
                eventsChannel.send(
                    AllEvents.Error(Constants.EMPTY_TYPE)
                )
            description == "" ->
                eventsChannel.send(
                    AllEvents.Error(Constants.EMPTY_DESCRIPTION)
                )
            description.length <= 10 ->
                eventsChannel.send(
                    AllEvents.Error(Constants.SHORT_DESCRIPTION)
                )
            else -> createStationReport(
                dateTime,
                description,
                type,
                station
            )
        }
        isLoading.postValue(false)
    }

    private fun createStationReport(
        dateTime: Timestamp,
        description: String,
        type: String,
        station: String
    ) = viewModelScope.launch {
        withContext(Dispatchers.Default) {
            try {
                val trimstation = station.filter { !it.isWhitespace() }
                val createdStationReport = reportRepository.createStationReportInDatabase(
                    dateTime,
                    description,
                    type,
                    trimstation
                )
                lastStationReport.postValue(createdStationReport)
                currentReportModel.postValue(null)
                reportRepository.clearCurrentStationReport()
                eventsChannel.send(AllEvents.Message("CreateStationReport: Success"))
            } catch (e: Exception) {
                val error = e.toString().split(":").toTypedArray()
                Log.d("ERROR", "CreateStationReportError: ${error[1]}")
                eventsChannel.send(AllEvents.Error(error[1]))
            }
        }
    }

    fun validateAndGetReportsFromLastHourByStation(stationName : String, stationName2: String) = viewModelScope.launch {
        when {
            stationName.isEmpty() -> eventsChannel.send(AllEvents.Error(Constants.EMPTY_STAION_NAME_ERROR))
            stationName2.isEmpty() -> eventsChannel.send(AllEvents.Error(Constants.EMPTY_STAION_NAME_ERROR))
            else ->  getReportsFromLastHourByStation(stationName,stationName2)
        }
    }

    private fun getReportsFromLastHourByStation(stationName: String, stationName2: String) = viewModelScope.launch {
        val reports = reportRepository.getReportsFromLastHourByStation(stationName.replace(" ",""))
        val reports2 = reportRepository.getReportsFromLastHourByStation(stationName2.replace(" ",""))

        reportsQuantity.postValue(mapOf(
            stationName to reports.size,
            stationName2 to reports2.size
        ))
    }

    sealed class AllEvents {
        data class Message(val message : String) : AllEvents()
        data class Error(val error : String) : AllEvents()
    }
}