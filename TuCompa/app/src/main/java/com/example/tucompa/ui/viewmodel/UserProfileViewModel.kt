package com.example.tucompa.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tucompa.data.repository.BaseAuthenticator
import com.example.tucompa.data.repository.UserRepository
import com.example.tucompa.domain.Route
import com.example.tucompa.domain.User
import com.example.tucompa.domain.UserRating
import com.google.firebase.Timestamp
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.sql.Connection
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@HiltViewModel
class UserProfileViewModel @Inject constructor(
    val authenticator: BaseAuthenticator,
    private val userRepository: UserRepository
) :
    ViewModel() {
    val currentUserModel = MutableLiveData<User?>()

    val lastFetchedUser = MutableLiveData<User?>()

    private val acceptedUsersList = MutableLiveData<List<User>>()
    val acceptedUsers get() = acceptedUsersList

    private val pendingUsersList = MutableLiveData<List<User>>()
    val pendingUsers get() = pendingUsersList

    private val routesAndUsersList = MutableLiveData<MutableList<User>?>()
    val routesAndUsers get() = routesAndUsersList

    private val isLoading = MutableLiveData<Boolean>()
    val loading get() = isLoading

    fun getUser(email: String, isConnection: Boolean) =
        viewModelScope.launch {
            val user = userRepository.getUser(email ?: "", isConnection)
            lastFetchedUser.postValue(user)
        }


    fun currentUser(isConnection: Boolean) =
        viewModelScope.launch {
            isLoading.postValue(true)
            Log.i("UserProfileViewModel", "Trying to fetch current user from Firestore")
            val user = authenticator.getUser()
            Log.i("UserProfileViewModel", "Succeed fetching user from Firestore $user")
            var email: String? = null

            if (user != null) {
                email = user.email
            }
            val currentUser = userRepository.getCurrentUser(email, isConnection)
            currentUserModel.postValue(currentUser)
            isLoading.postValue(false)
        }


    fun signOut(){
        viewModelScope.launch {
            val user = authenticator.signOut()
            userRepository.deleteCurrentUser()
            currentUserModel.postValue(null)
        }
    }

    fun getUsersFromRouteList(routes : List<Route>, isConnection: Boolean) = viewModelScope.launch {
        var users : ArrayList<User> = arrayListOf<User>()
        routes.forEach { r ->
            users.add(userRepository.getUser(r.userEmail, isConnection))
        }
        routesAndUsersList.postValue(users)
    }

    fun getAcceptedUsersFromUserRatings(ratings : List<UserRating>, isConnection: Boolean) = viewModelScope.launch {
        var users : ArrayList<User> = arrayListOf<User>()
        ratings.forEach { r ->
            users.add(userRepository.getUser(r.userEmail,isConnection))
        }
        acceptedUsersList.postValue(users)
    }

    fun getPendingUsersFromUserRatings(ratings : List<UserRating>, isConnection: Boolean) = viewModelScope.launch {
        var users : ArrayList<User> = arrayListOf<User>()
        ratings.forEach { r ->
            users.add(userRepository.getUser(r.userEmail, isConnection))
        }
        pendingUsersList.postValue(users)
    }

    fun updateIsInRoute(userEmail: String, newIsInRoute: Boolean, newRouteId : String) = viewModelScope.launch {
        userRepository.updateUserIsInRoute(userEmail, newIsInRoute)
        userRepository.updateUserActualRoute(userEmail, newRouteId)
    }

    fun updateLoggedUserIsInRoute(newIsInRoute: Boolean, newRouteId : String) = viewModelScope.launch {
        val user = authenticator.getUser()
        if (user != null) {
            val email = user.email
            if (email != null) {
                val userEmail = userRepository.getUser(email, true).email

                userRepository.updateUserIsInRoute(userEmail, newIsInRoute)

                if(newIsInRoute) {
                    userRepository.updateUserActualRoute(userEmail, newRouteId)
                }
            }
        }
    }

    fun updateRate(userEmail: String, newRate: Float) = viewModelScope.launch {
        val user = userRepository.getUser(userEmail, true)
        val newQuantityRating = user.quantityRating + 1
        val newTotalRate = (user.totalRating + newRate).toFloat()

        Log.i("UserProfileViewModel","Updating user $userEmail total rating to $newTotalRate after adding rate $newRate")

        userRepository.updateUserQuantityRating(userEmail, newQuantityRating)
        userRepository.updateUserRating(userEmail, newTotalRate)
    }

    fun updateLastRouteAndStreak(newLastRoute: Timestamp) = viewModelScope.launch {
        val user = authenticator.getUser()
        var email: String? = null
        var dbUser : User? = null
        if (user != null) {
            email = user.email
            if (email != null) {
                dbUser = userRepository.getUser(email, true)
                val prevRoute = dbUser.lastRoute

                //Update lastRoute
                userRepository.updateUserLastRouteDate(email, newLastRoute)

                val cal = Calendar.getInstance()
                val cal2 = Calendar.getInstance()

                cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - 1)
                if(Timestamp(Date(cal.timeInMillis)).compareTo(prevRoute) == -1) {
                    cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) + 1)
                    cal2.timeInMillis = prevRoute?.toDate()?.time ?: cal2.timeInMillis
                    if(cal.get(Calendar.DAY_OF_YEAR) != cal2.get(Calendar.DAY_OF_YEAR)) {
                        userRepository.updateUserStreak(email, (dbUser?.streak ?: 0) + 1 )
                    }
                }
                else {
                    userRepository.updateUserStreak(email, 0)
                }
            }
        }
    }
}