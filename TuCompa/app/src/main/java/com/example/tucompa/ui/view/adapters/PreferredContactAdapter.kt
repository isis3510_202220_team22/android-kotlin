package com.example.tucompa.ui.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tucompa.R
import com.example.tucompa.databinding.ItemPreferredContactBinding
import com.example.tucompa.domain.PreferredContact
import com.squareup.picasso.Picasso

class PreferredContactAdapter (private var preferredContactList: List<PreferredContact>): RecyclerView.Adapter<PreferredContactAdapter.PreferredContactViewHolder>() {

    inner class PreferredContactViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemPreferredContactBinding.bind(view)

        fun render(preferredContact: PreferredContact){
            binding.preferredContactName.text = preferredContact.name
            binding.preferredContactNumber.text = preferredContact.number
            Picasso.get().load(preferredContact.image).into(binding.preferredContactPhoto)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PreferredContactViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_preferred_contact, parent, false)
        return PreferredContactViewHolder(view)
    }

    override fun onBindViewHolder(holder: PreferredContactViewHolder, position: Int) {
        val preferredContact : PreferredContact = preferredContactList[position]
        holder.render(preferredContact)
    }

    override fun getItemCount(): Int = preferredContactList.size

    fun setActualPreferredContactsList(newPreferredContactsList: List<PreferredContact>) {
        preferredContactList = newPreferredContactsList
        notifyDataSetChanged()
    }
}