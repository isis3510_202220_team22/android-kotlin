package com.example.tucompa.data.repository

import android.util.Log
import com.example.tucompa.data.database.AppDatabase
import com.example.tucompa.data.database.entitites.toCurrentUserEntity
import com.example.tucompa.data.database.entitites.toEntity
import com.example.tucompa.data.model.UserModel
import com.example.tucompa.data.network.UserService
import com.example.tucompa.domain.User
import com.example.tucompa.domain.toDomain
import com.google.firebase.Timestamp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserRepository @Inject constructor(private val userService: UserService, private val appDatabase: AppDatabase){

    private val userDao = appDatabase.userDao()
    private val currentUserDao = appDatabase.currentUserDao()

    suspend fun getUser(email: String, isConnection: Boolean): User {
        val user : User = withContext(Dispatchers.IO) {
            if (isConnection) {
                val response: UserModel? = userService.getUser(email)

                if (response != null) {
                    userDao.insertAll(response.toEntity())
                    response.toDomain()
                } else {
                    val dbRsponse = userDao.getUserById(email)

                    if(dbRsponse != null) {
                        dbRsponse.toDomain()
                    } else {
                        User()
                    }
                }
            } else {
                val dbRsponse = userDao.getUserById(email)

                if(dbRsponse != null) {
                    dbRsponse.toDomain()
                } else {
                    User()
                }
            }
        }

        return user
    }

    suspend fun getCurrentUser(email: String?, isConnection : Boolean): User? {
        val user : User? = withContext(Dispatchers.IO) {
            if(email != null && isConnection) {
                val response: UserModel? = userService.getUser(email)

                Log.i("UserRepository", "Got user $response from server")

                if (response != null) {
                    currentUserDao.insertAll(response.toCurrentUserEntity())
                    response.toDomain()
                } else {
                    val dbResponse = currentUserDao.getAll()
                    Log.i("UserRepository", "Got user $dbResponse from Room")

                    if(dbResponse.isNotEmpty()) {
                        dbResponse[0].toDomain()
                    } else {
                        null
                    }
                }
            } else {
                val dbResponse = currentUserDao.getAll()
                Log.i("UserRepository", "Got user $dbResponse from Room")

                if(dbResponse.isNotEmpty()) {
                    dbResponse[0].toDomain()
                } else {
                    null
                }
            }
        }

        return user
    }

    suspend fun deleteCurrentUser() {
        currentUserDao.deleteAll()
    }

    suspend fun updateUserLastRouteDate(email : String, newLastRoute: Timestamp): User? {
        return userService.updateUserLastRouteDate(email, newLastRoute)?.toDomain()
    }

    suspend fun updateUserStreak(email : String, newStreak: Int): User? {
        return userService.updateUserStreak(email, newStreak)?.toDomain()
    }

    suspend fun updateUserIsInRoute(email: String, newIsInRoute: Boolean) : User? {
        return userService.updateUserIsInRoute(email,newIsInRoute)?.toDomain()
    }

    suspend fun updateUserActualRoute(email: String, newRouteId: String) : User? {
        return userService.updateUserActualRoute(email,newRouteId)?.toDomain()
    }

    suspend fun updateUserRating(email: String, newRating: Float) : User? {
        return userService.updateUserRating(email, newRating)?.toDomain()
    }

    suspend fun updateUserQuantityRating(email: String, newQRating: Int) : User? {
        return userService.updateUserQuantityRating(email,newQRating)?.toDomain()
    }
}