package com.example.tucompa.data.model

data class PreferredContactModel(
    val name: String = "",
    val number: String = "",
    val image: String = ""
)
