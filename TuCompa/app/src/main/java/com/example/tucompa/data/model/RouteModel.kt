package com.example.tucompa.data.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId

data class RouteModel(
    @DocumentId val id : String = "",
    val departureDate: Timestamp = Timestamp.now(),
    val destinationStationName: String = "",
    val originStationName : String = "",
    val userEmail: String = "",
    @field:JvmField val isFinished : Boolean = false
)