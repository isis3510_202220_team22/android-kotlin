package com.example.tucompa.ui.view

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tucompa.Constants
import com.example.tucompa.R
import com.example.tucompa.connectivity.ConnectivityLiveData
import com.example.tucompa.databinding.FragmentPreferredContactsBinding
import com.example.tucompa.databinding.FragmentSearchBinding
import com.example.tucompa.ui.view.adapters.PreferredContactAdapter
import com.example.tucompa.ui.viewmodel.PreferredContactViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PreferredContactsFragment : Fragment(R.layout.fragment_preferred_contacts) {

    private lateinit var binding: FragmentPreferredContactsBinding
    private var activity: AppActivity? = null

    private val preferredContactViewModel: PreferredContactViewModel by viewModels()

    private lateinit var preferredContactAdapter: PreferredContactAdapter

    private lateinit var connectivityLiveData: ConnectivityLiveData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPreferredContactsBinding.inflate(inflater, container, false)

        activity = getActivity() as AppActivity

        connectivityLiveData = context?.let { ConnectivityLiveData(it) }!!

        val bar: View? = activity?.findViewById(R.id.lyBack) ?: null
        if (bar != null) {
            bar.visibility = View.VISIBLE
        }

        val tvTop: TextView? = activity?.findViewById(R.id.txtNavTittle) ?: null
        if (tvTop != null) {
            tvTop.text = "Contactos favoritos"
        }

        addObservers()
        setup()
        initRecyclerView()

        return binding.root
    }

    private fun initRecyclerView() {
        val recyclerView = binding.recyclerPreferredContacts
        val manager = LinearLayoutManager(activity)

        recyclerView.layoutManager = manager

        preferredContactAdapter = PreferredContactAdapter(listOf())

        recyclerView.adapter = preferredContactAdapter
    }

    private fun setup() {
        binding.apply {
            idFABAdd.setOnClickListener {
                replaceFragment(AddPreferredContactFragment())
            }
        }
    }

    private fun addObservers() {
        preferredContactViewModel.getPreferredContacts()

        preferredContactViewModel.preferredContactsList.observe(viewLifecycleOwner){
            if (it != null) {
                if (it.isEmpty()) {
                    binding.noPreferredContactsFound.visibility = View.VISIBLE
                    preferredContactAdapter.setActualPreferredContactsList(listOf())
                } else {
                    binding.noPreferredContactsFound.visibility = View.GONE
                    preferredContactAdapter.setActualPreferredContactsList(it)
                }
            }
        }

        connectivityLiveData.checkValidNetworks()

        connectivityLiveData.observe(viewLifecycleOwner) { isNetwork ->
            if (!isNetwork){
                openDialog(Constants.ADD_PREFERRED_CONTACT_CONNECTION_ERROR)
            }
        }
    }

    private fun replaceFragment(fragment: Fragment){
        val fragmentTransaction = getActivity()?.supportFragmentManager?.beginTransaction()
        fragmentTransaction?.replace(R.id.container, fragment)
        fragmentTransaction?.addToBackStack(null)
        fragmentTransaction?.commit()
    }

    private fun openDialog(body: String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Sin conexion").setMessage(body)
            .setPositiveButton("OK") { _, _ -> }
        val dialogo = builder.create()
        dialogo.show()
    }

}