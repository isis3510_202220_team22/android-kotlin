package com.example.tucompa.data.repository

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import kotlinx.coroutines.tasks.await

class FirebaseAuthenticator : BaseAuthenticator {
    override suspend fun signUpWithEmailPassword(email: String, password: String): FirebaseUser? {
        return withContext(IO){
            Firebase.auth.createUserWithEmailAndPassword(email,password).await()
            Firebase.auth.currentUser
        }
    }

    override suspend fun signInWithEmailPassword(email: String, password: String): FirebaseUser? {
        return withContext(IO){
            Firebase.auth.signInWithEmailAndPassword(email , password).await()
            Firebase.auth.currentUser
        }
    }

    override fun signOut(): FirebaseUser? {
        Firebase.auth.signOut()
        return Firebase.auth.currentUser
    }

    override fun getUser(): FirebaseUser? {
        return Firebase.auth.currentUser
    }

}