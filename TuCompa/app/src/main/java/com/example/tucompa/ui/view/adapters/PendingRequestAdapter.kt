package com.example.tucompa.ui.view.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tucompa.R
import com.example.tucompa.databinding.PendingRequestBinding
import com.example.tucompa.domain.User
import com.example.tucompa.domain.UserRating
import com.example.tucompa.ui.view.listeners.CommuteListener
import com.squareup.picasso.Picasso
import kotlin.math.round

class PendingRequestAdapter(private var requestList: MutableList<UserRating>, private var userList: List<User>, private val listener : CommuteListener) :
    RecyclerView.Adapter<PendingRequestAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = PendingRequestBinding.bind(view)

        fun setListener(userRating: UserRating) {
            binding.btnAccept.setOnClickListener {
                listener.onAcceptButton(userRating)
            }
            binding.btnDecline.setOnClickListener {
                listener.onDeclineButton(userRating)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.pending_request, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user : User = userList[position]
        val userRating : UserRating = requestList[position]

        holder.setListener(userRating)
        holder.binding.profileName.text = user.name
        holder.binding.profileRating.text = (round(user.totalRating*100/user.quantityRating) /100).toString()
        Picasso.get().load(user.image).into(holder.binding.profilePhoto)

    }

    fun addRequests(newRequest:  MutableList<UserRating>, newUser: List<User>) {
        Log.i("AcceptedRequestsAdapter", "Updating data request: $newRequest; user: $newUser")
        requestList = newRequest
        userList = newUser
        Log.i("AcceptedRequestsAdapter", "Updated data request: $newRequest; user: $newUser")
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = requestList.size
}