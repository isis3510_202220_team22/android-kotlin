package com.example.tucompa.data.database.dao

import androidx.room.*
import com.example.tucompa.data.database.entitites.UserEntity

@Dao
interface UserDao {
    @Query("SELECT * FROM user")
    suspend fun getAll(): List<UserEntity>

    @Query("SELECT * FROM user WHERE email = :userEmail")
    suspend fun getUserById(userEmail: String): UserEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(vararg users: UserEntity)

    @Delete
    suspend fun delete(user: UserEntity)
}