package com.example.tucompa.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tucompa.Constants
import com.example.tucompa.data.repository.RouteRepository
import com.example.tucompa.data.repository.StationRepository
import com.example.tucompa.domain.Route
import com.example.tucompa.domain.User
import com.example.tucompa.domain.UserRating
import com.google.firebase.Timestamp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject

@HiltViewModel
class RoutesViewModel @Inject constructor(
    private val routeRepository: RouteRepository,
    private val stationRepository: StationRepository,
    private val firebaseAnalytics: FirebaseAnalytics
) : ViewModel() {

    //private val firebaseAnalytics= Firebase.analytics

    private val createRouteEventsChannel = Channel<AllEvents>()
    val allCreateRouteEventsFlow = createRouteEventsChannel.receiveAsFlow()

    private val getRouteEventsChannel = Channel<AllEvents>()
    val allGetRouteEventsFlow = getRouteEventsChannel.receiveAsFlow()

    private val userRatingEventsChannel = Channel<AllEvents>()
    val allUserRatingEventsFlow = userRatingEventsChannel.receiveAsFlow()

    val allRoutes = MutableLiveData<MutableList<Route>?>()

    val allRoutesConditional = MutableLiveData<MutableList<Route>?>()

    private val userRoutes = MutableLiveData<List<Route>>()
    val currentUserRoutes get() = userRoutes

    private val lastCreatedRoute = MutableLiveData<Route>()
    val lastRoute get() = lastCreatedRoute

    private val actualUserRating = MutableLiveData<UserRating>()
    val userRating get() = actualUserRating

    private val searchUserRating = MutableLiveData<UserRating?>()
    val foundUserRating get() = searchUserRating

    private val allUserRatings = MutableLiveData<MutableList<List<UserRating>>>()
    val allUsersRatingsFound get() = allUserRatings

    private val pendingUserRatings = MutableLiveData<List<UserRating>>()
    val pendingUsers get() = pendingUserRatings

    private val acceptedUserRatings = MutableLiveData<List<UserRating>>()
    val acceptedUsers get() = acceptedUserRatings

    private val isLoading = MutableLiveData<Boolean>()
    val loading get() = isLoading

    private val isSearching = MutableLiveData<Boolean>(false)
    val searching get() = isSearching

    fun getRoute(routeId: String, isConnectivity: Boolean) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            isLoading.postValue(true)
            var route = routeRepository.getRoute(routeId, isConnectivity)
            route?.let {
                lastRoute.postValue(route)
            }
            isLoading.postValue(false)
        }
    }

    fun deleteRoute(routeId: String) = viewModelScope.launch {
        routeRepository.deleteRouteFromDatabase(routeId)
    }

    fun finishRoute(routeId: String) = viewModelScope.launch {
        routeRepository.updateRouteIsFinishedInDatabase(routeId, true)
    }

    fun getAllRoutesUserIsBeen(userEmail: String, isConnection: Boolean) = viewModelScope.launch {
        isLoading.postValue(true)
        currentUserRoutes.postValue(routeRepository.getAllRoutesUserIsBeen(userEmail, isConnection))
        isLoading.postValue(false)
    }

    fun getUserRatingsListFromRoutesList(routesList: MutableList<Route>?, isConnectivity : Boolean) = viewModelScope.launch{
        withContext(Dispatchers.Default){
            isLoading.postValue(true)
            if (routesList != null) {
                var fullRatingList = mutableListOf<List<UserRating>>()
                routesList.forEach {
                    var ratingList = routeRepository.getUserRatingsFromRouteFromDatabase(it.id, isConnectivity)
                    fullRatingList.add(ratingList)
                }
                Log.d("listadelistas", "${fullRatingList}")
                allUserRatings.postValue(fullRatingList)
            }
            isLoading.postValue(false)
        }
    }

    fun getUserRatings(routeId: String, isConnection: Boolean) = viewModelScope.launch {
        withContext(Dispatchers.IO) {
            isLoading.postValue(true)
            var userRatings = routeRepository.getUserRatingsFromRouteFromDatabase(routeId, isConnection)
            var pending = arrayListOf<UserRating>()
            var accepted = arrayListOf<UserRating>()

            userRatings.forEach { userRating ->
                if (userRating.pending) {
                    pending.add(userRating)
                } else {
                    accepted.add(userRating)
                }
            }
            pendingUserRatings.postValue(pending.toList())
            acceptedUserRatings.postValue(accepted.toList())
            isLoading.postValue(false)
        }
    }

    fun createUserRating(
        routeId: String,
        pending: Boolean,
        userEmail: String
    ) =
        viewModelScope.launch {
            val createdUserRating =
                routeRepository.createUserRatingInDatabase(routeId, pending, userEmail)

            if (createdUserRating == null) {
                userRatingEventsChannel.send(AllEvents.Error(Constants.ERROR_POSTING_USER_RATING))
            } else {
                actualUserRating.postValue(createdUserRating!!)
            }
        }

    fun acceptUserRating(routeId: String, userEmail: String) = viewModelScope.launch {
        isLoading.postValue(true)
        val acceptedUserRating =
            routeRepository.updateUserRatingPendingInDatabase(routeId, userEmail, false)
        getUserRatings(routeId, true)
        acceptedUserRating?.let {
            Log.i("RoutesViewModel", "Successfully accepted UserRating $userEmail")
        }
        isLoading.postValue(false)
    }

    fun deleteUserRating(routeId: String, userEmail: String) = viewModelScope.launch {
        isLoading.postValue(true)
        val deletedUserRating = routeRepository.deleteUserRatingFromDatabase(routeId, userEmail)
        getUserRatings(routeId, true)
        deletedUserRating?.let {
            Log.i("RoutesViewModel", "Successfully deleted UserRating $userEmail")
        }
        isLoading.postValue(false)
    }

    fun updateUserRatingRate(routeId: String, userEmail: String, newRate: Float) =
        viewModelScope.launch {
            isLoading.postValue(true)
            val deletedUserRating =
                routeRepository.updateUserRatingRateInDatabase(routeId, userEmail, newRate)
            deletedUserRating?.let {
                Log.i(
                    "RoutesViewModel",
                    "Successfully updated UserRating $userEmail rate to $newRate"
                )
            }
            isLoading.postValue(false)
        }

    fun validateAndGetRoutesConditional(
        date: Timestamp?,
        destinationStationName: String?,
        originStationName: String?,
        isConnection: Boolean
    ) {
        viewModelScope.launch {
            isSearching.postValue(true)
            val stations = stationRepository.getStations(isConnection).map { station -> station.name }

            var date1 = Calendar.getInstance()
            date1.time = date!!.toDate()
            date1.add(Calendar.MINUTE, -5)
            var date2 = Timestamp(Date(date1.timeInMillis))

            if (date != null && date.compareTo(date2) < 1) {
                getRouteEventsChannel.send(
                    AllEvents.Error(Constants.PAST_DEPARTURE_TIME_ERROR)
                )
            } else if (destinationStationName != null && originStationName != null && destinationStationName == originStationName) {
                getRouteEventsChannel.send(
                    AllEvents.Error(Constants.SAME_STATION_ERROR)
                )
            } else if (originStationName != null && originStationName !in stations) {
                getRouteEventsChannel.send(
                    AllEvents.Error(Constants.NON_EXISTENT_ORIGIN_STATION_ERROR)
                )
            } else if (destinationStationName != null && destinationStationName !in stations) {
                getRouteEventsChannel.send(
                    AllEvents.Error(Constants.NON_EXISTENT_DESTINATION_STATION_ERROR)
                )
            } else {
                getRoutesConditional(
                    date,
                    destinationStationName,
                    originStationName
                )
            }
            isSearching.postValue(false)
        }
    }

    fun getRoutesConditional(
        date: Timestamp?,
        destinationStationName: String?,
        originStationName: String?
    ) {
        val trimstationd = destinationStationName!!.filter { !it.isWhitespace() }
        val trimstationo = originStationName!!.filter { !it.isWhitespace() }

        firebaseAnalytics.logEvent("stationsearch"){
            param("destinationstation", trimstationd.lowercase())
            param("originstation", trimstationo.lowercase())
        }

        viewModelScope.launch {
            val routes = routeRepository.getRoutesConditional(
                date,
                destinationStationName,
                originStationName
            )
            allRoutesConditional.postValue(routes)
        }
    }

    fun logRecommendedRouteEvent(userEmail: String) {
        Log.i("RoutesViewModel", "Logging smart feature event to user $userEmail")
        firebaseAnalytics.logEvent("smart_feature_used"){
            param("User", userEmail)
        }
    }

    fun logCommuteViewOpened(userEmail: String) {
        Log.i("RoutesViewModel", "Logging commute view opened event to user $userEmail")
        firebaseAnalytics.logEvent("commute_view_opened"){
            param("User", userEmail)
        }
    }

    fun validateAndCreateRoute(
        departureDate: Timestamp,
        destinationStationName: String,
        originStationName: String,
        userEmail: String,
        isConnection: Boolean
    ) = viewModelScope.launch {
        isLoading.postValue(true)
        val stations = stationRepository.getStations(isConnection).map { station -> station.name }
        when {
            departureDate.compareTo(Timestamp.now()) < 1 ->
                createRouteEventsChannel.send(
                    AllEvents.Error(Constants.PAST_DEPARTURE_TIME_ERROR)
                )
            originStationName !in stations ->
                createRouteEventsChannel.send(
                    AllEvents.Error(Constants.NON_EXISTENT_ORIGIN_STATION_ERROR)
                )
            destinationStationName !in stations ->
                createRouteEventsChannel.send(
                    AllEvents.Error(Constants.NON_EXISTENT_DESTINATION_STATION_ERROR)
                )
            (originStationName == destinationStationName) ->
                createRouteEventsChannel.send(
                    AllEvents.Error(Constants.SAME_STATION_ERROR)
                )
            else -> createRoute(
                departureDate,
                destinationStationName,
                originStationName,
                userEmail
            )
        }
        isLoading.postValue(false)
    }

    private fun createRoute(
        departureDate: Timestamp,
        destinationStationName: String,
        originStationName: String,
        userEmail: String
    ) = viewModelScope.launch {
        withContext(Dispatchers.Default) {
            try {
                val createdRoute = routeRepository.createRoute(
                    departureDate,
                    destinationStationName,
                    originStationName,
                    userEmail
                )
                lastRoute.postValue(createdRoute)
                Log.i("RoutesViewModel:", "Posted route $createdRoute")
                if (createdRoute != null) {
                    createRouteEventsChannel.send(AllEvents.Message("CreateCommute: Success"))
                    createUserRating(createdRoute.id, false, userEmail)
                } else {
                    createRouteEventsChannel.send(AllEvents.Error("Error creating route"))
                }
            } catch (e: Exception) {
                val error = e.toString().split(":").toTypedArray()
                Log.d("ERROR", "CreateCommuteError: ${error[1]}")
                createRouteEventsChannel.send(AllEvents.Error(error[1]))
            }
        }
    }

    sealed class AllEvents {
        data class Message(val message: String) : AllEvents()
        data class Error(val error: String) : AllEvents()
    }
}