package com.example.tucompa.data.network

import com.example.tucompa.Constants
import com.example.tucompa.data.model.RouteModel
import com.example.tucompa.data.model.StationModel
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class StationService @Inject constructor(private val db: FirebaseFirestore) {

    private val collectionStation = db.collection(Constants.STATION_COLLECTION_NAME)

    suspend fun getStations() : List<StationModel>? {
        var stations : List<StationModel>? = withContext(Dispatchers.IO){
            collectionStation.get().await().toObjects(StationModel::class.java)
        }
        return stations
    }

    suspend fun getStation(stationName : String) : StationModel? {
        var station : StationModel? = null
        val job = GlobalScope.launch {
            station =
                collectionStation.document(stationName).get().await().toObject(StationModel::class.java)
        }

        runBlocking {
            job.join()
        }

        return station
    }
}