package com.example.tucompa.domain

import com.example.tucompa.data.database.entitites.RouteEntity
import com.example.tucompa.data.model.RouteModel
import com.google.firebase.Timestamp

data class Route(
    val id : String,
    val departureDate: Timestamp,
    val destinationStationName: String,
    val originStationName : String,
    val userEmail: String,
    val isFinished : Boolean
)

fun RouteModel.toDomain() = Route(id, departureDate, destinationStationName, originStationName, userEmail, isFinished)
fun RouteEntity.toDomain() = Route(id, Timestamp(departureDate), destinationStationName, originStationName, userEmail, isFinished)