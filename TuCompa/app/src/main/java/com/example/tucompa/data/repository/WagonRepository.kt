package com.example.tucompa.data.repository

import com.example.tucompa.data.model.WagonModel
import com.example.tucompa.data.network.WagonService
import com.example.tucompa.domain.Wagon
import com.example.tucompa.domain.toDomain
import javax.inject.Inject

class WagonRepository @Inject constructor(private val wagonService: WagonService) {

    fun getWagon(name: String, id: String): Wagon? {
        val response: WagonModel? = wagonService.getWagon(name, id)

        if (response != null) {
            return response.toDomain()
        } else {
            return null
        }
    }
}