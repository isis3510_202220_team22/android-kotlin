package com.example.tucompa.data.database.entitites

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.tucompa.data.model.UserRatingModel
import java.util.*

@Entity(tableName = "user_rating")
data class UserRatingEntity (
    @PrimaryKey @ColumnInfo(name = "user_email") val userEmail: String,
    @ColumnInfo(name = "rate") val rate: Float,
    @ColumnInfo(name = "pending") val pending: Boolean,
    @ColumnInfo(name = "inadequate") val inadequate: Boolean,
)

fun UserRatingModel.toEntity() = UserRatingEntity(userEmail, rate, pending, inadequate)
